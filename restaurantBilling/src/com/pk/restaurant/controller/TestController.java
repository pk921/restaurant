package com.pk.restaurant.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.pk.restaurant.entity.Customer;
import com.pk.restaurant.entity.Employee;
import com.pk.restaurant.entity.Restaurant;
import com.pk.restaurant.service.EmployeeService;
import com.pk.restaurant.service.RestaurantService;

@Controller
@RequestMapping(value = "/test")
public class TestController {

	@Autowired
	private RestaurantService restaurantService;
	
	@Autowired
	private EmployeeService employeeService;
	
	//tested
	@RequestMapping()
	public String test(Model model){
		
		model.addAttribute("restaurantAttribute", new Restaurant());
		model.addAttribute("employeeAttribute", new Employee());
		
		//return "test";
		model.addAttribute("page", "test");
		return "home";
	}
	
	@RequestMapping("restaurant")
	public String getEntity(@ModelAttribute("restaurantAttribute") Restaurant restaurant,
			Model model){
		
		int restaurantId = restaurant.getId();
		System.out.println(restaurantId);
		/*
		//Get Restaurant
		Restaurant tempRestaurant = restaurantService.getRestaurant(restaurantId);
		System.out.println(tempRestaurant);
		System.out.println(tempRestaurant.getMenuItems());
		model.addAttribute("restaurantAttribute", tempRestaurant);
		*/
		
		// delete restaurant
		restaurantService.removeRestaurant(restaurantId);
		model.addAttribute("restaurantAttribute", restaurant);
		
		
		model.addAttribute("employeeAttribute", new Employee());
		//return "test";
		model.addAttribute("page", "test");
		return "home";
	}
	
	@RequestMapping("employee")
	public String getEntity(@ModelAttribute("employeeAttribute") Employee employee,
			Model model){
		
		int employeeId = employee.getId();
		System.out.println(employeeId);
		
		/*
		//Get Employee
		Employee tempEmployee = employeeService.getEmployee(employeeId);
		System.out.println(tempEmployee);
		model.addAttribute("employeeAttribute", tempEmployee);
		*/
		
		// delete employee
		employeeService.removeEmployee(employeeId);
		model.addAttribute("employeeAttribute", employee);
		
		model.addAttribute("restaurantAttribute", new Restaurant());
		//return "test";
		model.addAttribute("page", "test");
		return "home";
	}
}
