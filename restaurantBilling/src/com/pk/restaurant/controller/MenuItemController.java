package com.pk.restaurant.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/menuitems")
public class MenuItemController {

	@RequestMapping(value = "/view")
	public String viewMenuItems(Model model){	
		
		model.addAttribute("page", "menuitems");
		return "home";
	}	
}
