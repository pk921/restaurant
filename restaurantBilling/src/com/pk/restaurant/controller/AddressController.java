package com.pk.restaurant.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.pk.restaurant.entity.Address;
import com.pk.restaurant.entity.Customer;
import com.pk.restaurant.service.AddressService;
import com.pk.restaurant.service.CustomerService;

@Controller
@RequestMapping(value = "/address")
public class AddressController {

	@Autowired
	private AddressService addressService;
	
	@Autowired
	private CustomerService customerService;
	
	@RequestMapping("/addaddress")
	public String addaddress(@ModelAttribute("addressAttribute") Address address, 
			Model model){
		Customer customer = null;
		String success = "";
		String error = "" ;
		String errorMsg = "";
		int customerId = 0;
		try{
			customerId = address.getAddressPK().getCustomerId();
			Address tempAddress = addressService.addAddress(address);
			
			if(tempAddress == null){
				error = "Not able to Add Address!";
				customer = customerService.getCustomer(customerId);
			} else{
				success = "Address Added!";
				customer = tempAddress.getCustomer();
			}
		} catch(Exception e){
			error = "An Error Occured! ";
			errorMsg = e.toString();
			customer = customerService.getCustomer(customerId);
			e.printStackTrace();
		}
		
		model.addAttribute("addressAttribute", new Address());
		model.addAttribute("success", success);
		model.addAttribute("error", error + errorMsg);
		if(customer == null){
			error = "Customer not found! " + error;
			model.addAttribute("customerAttribute", new Customer());
			//return "customer";
			model.addAttribute("page", "customer");
			return "home";
		}
		else {
			model.addAttribute("customerAttribute", customer);
		}
		//return "searchcustomer";
		model.addAttribute("page", "searchcustomer");
		return "home";
	}

	@RequestMapping(value="/bill/addaddress", method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Address addCustomer(@RequestBody Address address){
		Address tempAddress = null;
		try{
			System.out.println(address);
			tempAddress = addressService.addAddress(address);
			System.out.println(tempAddress);
			if(tempAddress.getAddressPK() == null){
				return null;
			}
		}catch(Exception e){
				return null;
		}
		return tempAddress;
	}
	
	@RequestMapping(value="/bill/getcustomeraddresses/{custId}", method=RequestMethod.GET, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<Address> getCustomerAddressList(@PathVariable("custId") int custId){
		List<Address> addressList =  addressService.getCustomerAddressList(custId);
		return addressList;
	}
	
	@RequestMapping("/updateaddress")
	public String updateaddress(@ModelAttribute("addressAttribute") Address address, 
			Model model){
		Customer customer = null;
		String success = "";
		String error = "" ;
		String errorMsg = "";
		int customerId = 0;
		try{
			customerId = address.getAddressPK().getCustomerId();
			Address tempAddress = addressService.updateAddress(address);
			
			if(tempAddress == null){
				error = "Address not found!";
				customer = customerService.getCustomer(customerId);
			} else{
				success = "Address Updated!";
				customer = tempAddress.getCustomer();
			}
		} catch(Exception e){
			error = "An Error Occured! ";
			errorMsg = e.toString();
			customer = customerService.getCustomer(customerId);
			e.printStackTrace();
		}
		
		model.addAttribute("addressAttribute", new Address());
		model.addAttribute("success", success);
		model.addAttribute("error", error + errorMsg);
		if(customer == null){
			error = "Customer not found! " + error;
			model.addAttribute("customerAttribute", new Customer());
			//return "customer";
			model.addAttribute("page", "customer");
			return "home";
		}
		else {
			model.addAttribute("customerAttribute", customer);
		}
		//return "searchcustomer";
		model.addAttribute("page", "searchcustomer");
		return "home";
	}
	
	@RequestMapping("/removeaddress")
	public String removeAddress(@ModelAttribute("addressAttribute") Address address, 
			Model model){
		Customer customer = null;
		String success = "";
		String error = "" ;
		String errorMsg = "";
		int customerId = 0;
		try{
			customerId = address.getAddressPK().getCustomerId();
			addressService.removeAddress(address);
			
			customer = customerService.getCustomer(customerId);
			
			if(customer == null){
				error = "Address removed! Customer Not found!";
			} else{
				success = "Address removed!";
			}
		} catch(Exception e){
			error = "Could not remove the Address...";
			errorMsg = e.toString();
			customer = customerService.getCustomer(customerId);
			e.printStackTrace();
		}
		
		model.addAttribute("addressAttribute", new Address());
		model.addAttribute("success", success);
		model.addAttribute("error", error + errorMsg);
		if(customer == null){
			error = "Customer not found! " + error;
			model.addAttribute("customerAttribute", new Customer());
			//return "customer";
			model.addAttribute("page", "customer");
			return "home";
		}
		else {
			model.addAttribute("customerAttribute", customer);
		}
		//return "searchcustomer";
		model.addAttribute("page", "searchcustomer");
		return "home";
	}
}
