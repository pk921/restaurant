package com.pk.restaurant.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.pk.restaurant.entity.Bill;
import com.pk.restaurant.entity.BillItem;
import com.pk.restaurant.entity.Customer;
import com.pk.restaurant.entity.Employee;
import com.pk.restaurant.entity.Restaurant;
import com.pk.restaurant.service.BillService;
import com.pk.restaurant.service.CustomerService;

@Controller
@RequestMapping("bill")
public class BillController{
	
	@Autowired
	BillService billService;
	
	@Autowired
	private CustomerService customerService;	
	
	@RequestMapping("searchbill")
	public String searchBill(Model model){
		model.addAttribute("customerAttribute", new Customer());
		model.addAttribute("page", "searchbill");
		return "home";
	}
	
	@RequestMapping("searchbill/byno/{billno}")
	public String searchBillByNo(@PathVariable("billno") int billNo, Model model){
		
		Bill bill = billService.getBill(billNo);
		
		model.addAttribute("billAttribute", bill);		
		model.addAttribute("page", "displaybill");
		return "home";
	}

	@RequestMapping("searchbill/bycustomer/{days}")
	public String searchBillByCustomer(@ModelAttribute("customerAttribute") Customer customer,
			@PathVariable("days") int days, Model model){
		
		Customer tempCustomer = customerService.getCustomer(customer);
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, - days);
		Date date = cal.getTime();
		
		List<Bill> billList = billService.getBill(tempCustomer, date);
		
		model.addAttribute("customerAttribute", tempCustomer);
		model.addAttribute("billListAttribute", billList);		
		model.addAttribute("page", "displaycustomerbills");
		return "home";
	}
	
	@RequestMapping("new")
	public String newBill(Model model){
		model.addAttribute("billAttribute", new Bill());
		model.addAttribute("page", "generatebill");
		return "home";
	}

	@RequestMapping("order")
	public String order(Model model){
		model.addAttribute("billAttribute", new Bill());
		model.addAttribute("page", "order");
		return "home";
	}
	
	@RequestMapping("order/{index}")
	public String getOrderByIndex(@PathVariable("index") int index, Model model,
			HttpServletRequest request){
		HttpSession session = request.getSession();
		@SuppressWarnings("unchecked")
		List<Bill> orderList = ((List<Bill>) session.getAttribute("orderList"));
		Bill order = orderList.get(index);
		
		model.addAttribute("orderNo", order.getBillNo());
		model.addAttribute("billAttribute", order);
		model.addAttribute("page", "order");
		return "home";
	}
	
	@RequestMapping("order/save")
	public String saveOrder(@ModelAttribute("billAttribute") Bill order, Model model, HttpServletRequest request){
		
		HttpSession session = request.getSession();
		@SuppressWarnings("unchecked")
		List<Bill> orderList = ((List<Bill>) session.getAttribute("orderList")); 
		if(orderList == null){
			orderList = new ArrayList<Bill>();
			session.setAttribute("orderList", orderList);
		}
		
		Integer orderCount = (Integer) session.getAttribute("orderCount");
		if(orderCount == null){
			orderCount = 0;
		}
		
		if(order.getBillNo() == 0){
			orderCount++;
			session.setAttribute("orderCount", orderCount);
			order.setBillNo(orderCount);
			orderList.add(order);
		} else{
			ListIterator<Bill> orderIterator = orderList.listIterator();
			while (orderIterator.hasNext()) {
				Bill tempOrder = orderIterator.next();
				if(tempOrder.getBillNo() == order.getBillNo()){
					orderIterator.set(order);
				}
			}
		}
		session.setAttribute("orderList", orderList);
		model.addAttribute("success", "Bill Saved Successfully!");
		model.addAttribute("orderNo", order.getBillNo());
		model.addAttribute("billAttribute", order);
		model.addAttribute("page", "order");
		return "home";
	}
	
	@RequestMapping("order/delete")
	public String deleteOrder(@ModelAttribute("billAttribute") Bill order, Model model, HttpServletRequest request){
		
		int orderNo = order.getBillNo();
		
		if(orderNo > 0){
			HttpSession session = request.getSession();
			@SuppressWarnings("unchecked")
			List<Bill> orderList = ((List<Bill>) session.getAttribute("orderList")); 
			if(orderList != null){				
				Iterator<Bill> orderIterator = orderList.iterator();
				while (orderIterator.hasNext()) {
					Bill tempOrder = orderIterator.next();
					if(tempOrder.getBillNo() == orderNo){
						orderIterator.remove();
						break;
					}
				}
			}
			session.setAttribute("orderList", orderList);
		}
		
		model.addAttribute("success", "Bill Deleted Successfully!");
		model.addAttribute("billAttribute", new Bill());
		model.addAttribute("page", "order");
		return "home";
	}
	
	@RequestMapping("generate")
	public String generateBill(@ModelAttribute("billAttribute") Bill bill, Model model, HttpServletRequest request){
		
		HttpSession session = request.getSession();
		Employee employee = (Employee) session.getAttribute("employee");//TODO: Remove Hardcoding
		Restaurant restaurant = (Restaurant) session.getAttribute("restaurant");//TODO: Remove Hardcoding
		
		Date date = new Date();
			
		bill.setEmployee(employee);
		bill.setRestaurant(restaurant);
		if(bill.getCustomer() == null || bill.getCustomer().getId() == 0){
			Customer defaultCustomer =(Customer) session.getAttribute("defaultCustomer");
			bill.setCustomer(defaultCustomer);
		}
		bill.setDate(date);
		
		Bill tempBill = billService.saveBill(bill);

		model.addAttribute("success", "Bill Generated  Successfully!");
		model.addAttribute("billAttribute", tempBill);
		model.addAttribute("billItemAttribute", new BillItem());
		model.addAttribute("page", "displaybill");
		return "home";
	}

}
