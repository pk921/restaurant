package com.pk.restaurant.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.pk.restaurant.entity.Address;
import com.pk.restaurant.entity.Customer;
import com.pk.restaurant.service.CustomerService;

@Controller
@RequestMapping(value = "/customer")
public class CustomerController {

	@Autowired
	private CustomerService customerService;
	
	//tested
	@RequestMapping("/home")
	public String getCustomer(Model model){
		
		Customer customer = new Customer();
		model.addAttribute("customerAttribute", customer);
		
		//return "customer";
		model.addAttribute("page", "customer");
		return "home";
	}
	
	/*
	//tested
	@RequestMapping("/request")
	public String requestHandler(@RequestParam(value="action", required=false) String action, @ModelAttribute("customerAttribute") Customer customer, Model model){

		if(action == null){
			return "customer";
		}
		
		//TODO: Remove Hard codings		
		switch(action){
			case "Add":	return "forward:/customer/addcustomer";
			case "Search":	return "forward:/customer/searchcustomer";
			case "Update": return "forward:/customer/updatecustomer";
			//case "Delete": return "forward:/customer/deletecustomer";
			//default: return "forward:/customer/list";
		}
		
		return "customer";
	}
	*/
	
	//tested
	@RequestMapping("/addcustomer")
	public String addCustomer(@ModelAttribute("customerAttribute") Customer customer,
			Model model){
		String success = "";		
		try{		
			customer = customerService.addCustomer(customer);
			
			if(customer == null){
				String error = "Not able to Add Customer!";
				model.addAttribute("error", error);
				model.addAttribute("customerAttribute", new Customer());
				//return "customer";
				model.addAttribute("page", "customer");
				return "home";
			} else{
				success = "Customer Added Successfully!";
			}
		}
		catch(Exception e){
			String error = "Could not Add Customer...";
			model.addAttribute("error", error + e.toString());
			model.addAttribute("customerAttribute", new Customer());
			e.printStackTrace();
			//return "customer";
			model.addAttribute("page", "customer");
			return "home";
		}

		model.addAttribute("customerAttribute", customer);
		model.addAttribute("addressAttribute", new Address());
		model.addAttribute("success", success);
		//return "searchcustomer";
		model.addAttribute("page", "searchcustomer");
		return "home";
	}
	
	//tested
	@RequestMapping(value="/bill/searchcustomer", method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Customer searchCustomer(@RequestBody Customer customer){
		Customer tempCustomer = null;
		try{
			tempCustomer = customerService.getCustomer(customer);
			System.out.println(tempCustomer);
		}catch(Exception e){
				return null;
		}
		return tempCustomer;
	}
	
	@RequestMapping(value="/bill/addcustomer", method=RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Customer addCustomer(@RequestBody Customer customer){
		Customer tempCustomer = null;
		try{
			tempCustomer = customerService.addCustomer(customer);
			System.out.println(tempCustomer);
		}catch(Exception e){
				return null;
		}
		return tempCustomer;
	}
	
	//tested
	@RequestMapping("/searchcustomer")
	public String searchCustomer(@ModelAttribute("customerAttribute") Customer customer,
			Model model){
		String error = "";
		try{
			Customer tempCustomer = customerService.getCustomer(customer);
			
			if(tempCustomer == null){
				error = "Customer Not found!";
				model.addAttribute("customerAttribute", customer);
				model.addAttribute("error", error);
				//return "customer";
				model.addAttribute("page", "customer");
				return "home";
			} else { // success
				model.addAttribute("customerAttribute", tempCustomer);
				model.addAttribute("addressAttribute", new Address());
				//return "searchcustomer";
				model.addAttribute("page", "searchcustomer");
				return "home";
			}
		}catch(Exception e){
			error = "Error in Searching the customer..."+e.toString();
			e.printStackTrace();
			model.addAttribute("customerAttribute", customer);
			model.addAttribute("error", error);
			//return "customer";
			model.addAttribute("page", "customer");
			return "home";
		}
	}
	
	//tested
	@RequestMapping("/updatecustomer")
	public String updateCustomer(@ModelAttribute("customerAttribute") Customer customer,
			Model model){
		
		try{
			Customer tempCustomer = customerService.updateCustomer(customer);
			
			if(tempCustomer == null){
				String error = "Customer Not found!";
				model.addAttribute("customerAttribute", customer);
				model.addAttribute("error", error);
				//return "customer";
				model.addAttribute("page", "customer");
				return "home";
			} else{
				String success = "Customer Updated!";
				model.addAttribute("customerAttribute", tempCustomer);
				model.addAttribute("success", success);
			}
			
		} catch(Exception e){
			String error = "Could not Update..." + e.toString();
			e.printStackTrace();
			model.addAttribute("customerAttribute", customer);
			model.addAttribute("error", error);
		}
		model.addAttribute("addressAttribute", new Address());
		//return "searchcustomer";
		model.addAttribute("page", "searchcustomer");
		return "home";
	}
	
	//tested
	@RequestMapping("/deletecustomer")
	public String deleteCustomer(@ModelAttribute("customerAttribute") Customer customer,
			Model model){

		
		try{
			customerService.deleteCustomer(customer);			
			String success = "Customer Deleted!";
			model.addAttribute("success", success);
			model.addAttribute("customerAttribute", customer);
			//return "customer";
			model.addAttribute("page", "customer");
			return "home";
			
		} catch(Exception e){
			String error = "Could not Delete..." + e.toString();
			e.printStackTrace();
			model.addAttribute("customerAttribute", customer);
			model.addAttribute("addressAttribute", new Address());
			model.addAttribute("error", error);
			//return "searchcustomer";
			model.addAttribute("page", "searchcustomer");
			return "home";
		}
	}

}
