package com.pk.restaurant.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.pk.restaurant.entity.Account;
import com.pk.restaurant.entity.Credentials;
import com.pk.restaurant.entity.Customer;
import com.pk.restaurant.entity.Employee;
import com.pk.restaurant.entity.MenuItem;
import com.pk.restaurant.entity.Restaurant;
import com.pk.restaurant.service.AccountService;
import com.pk.restaurant.service.CustomerService;
import com.pk.restaurant.service.EmployeeService;
import com.pk.utility.Constants;

@Controller
@RequestMapping(value = "account")
public class AccountController {
	
	@Autowired
	AccountService accountService;
	
	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	CustomerService customerService;

	@RequestMapping(method = RequestMethod.GET)
	public String index(){
		return "redirect:account/login.html"; 
	}
	
	
	@RequestMapping(value = "login", method = RequestMethod.GET)
	public String login(Model model, HttpServletRequest request){
		
		// get account details from cookie
		Account account = checkCookie(request);
		
		if(account == null){
			model.addAttribute("account", new Account());
			return "login";
		}
		else{
			Credentials credentials = accountService.login(account.getUsername(), account.getPassword());
			// validate account
			if(credentials != null){
				Employee employee = employeeService.getEmployee(credentials.getEmployeId());
				Customer defaultCustomer = customerService.getCustomer(1);
				request.getSession().setAttribute("employee", employee);
				request.getSession().setAttribute("restaurant", employee.getRestaurant());
				request.getSession().setAttribute("defaultCustomer", defaultCustomer);
				request.getSession().setAttribute("menuitems", getMenuItemsMap(employee.getRestaurant()));
				
				model.addAttribute("customerAttribute", new Customer());
				model.addAttribute("page", "customer");
				return "home";
			}
			else{
				model.addAttribute("error", "Account is invalid!");
				model.addAttribute("account", new Account());
				return "login";
			}
		}
	}
	
	@RequestMapping(value = "login", method = RequestMethod.POST)
	public String login(@ModelAttribute("account") Account account, Model model, 
			HttpServletRequest request, HttpServletResponse response){
		
		Credentials credentials = accountService.login(account.getUsername(), account.getPassword());
		// validate account
		if(credentials != null){
			Employee employee = employeeService.getEmployee(credentials.getEmployeId());
			Customer defaultCustomer = customerService.getCustomer(1);
			request.getSession().setAttribute("employee", employee);
			request.getSession().setAttribute("restaurant", employee.getRestaurant());
			request.getSession().setAttribute("defaultCustomer", defaultCustomer);
			request.getSession().setAttribute("menuitems", getMenuItemsMap(employee.getRestaurant()));

			// add user to cookie, if "remember" check-box is ticked
			if(request.getParameter("remember") != null){
				Cookie ck_username = new Cookie(Constants.username, account.getUsername());
				ck_username.setMaxAge(3600);
				response.addCookie(ck_username);
				
				Cookie ck_password = new Cookie(Constants.password, account.getPassword());
				ck_password.setMaxAge(3600);
				response.addCookie(ck_password);
			}
			model.addAttribute("customerAttribute", new Customer());
			model.addAttribute("page", "customer");
			return "home";
		}
		else{
			model.addAttribute("error", "Account is invalid!");
			model.addAttribute("account", new Account());
			return "login";
		}
		
	}
	
	@RequestMapping(value = "logout", method = RequestMethod.GET)
	public String logout(ModelMap modelMap, HttpServletRequest request, HttpServletResponse response){

		// remove cookie
		for(Cookie ck : request.getCookies()){			
			if(ck.getName().equalsIgnoreCase(Constants.username)){
				ck.setMaxAge(0);
				response.addCookie(ck);
			}
			else if(ck.getName().equalsIgnoreCase(Constants.password)){
				ck.setMaxAge(0);
				response.addCookie(ck);
			}			
		}
		//remove session
		request.getSession().invalidate();
		
		return "redirect:login.html";
	}
	
	public Account checkCookie(HttpServletRequest request){
		Account account = null;
		
		Cookie[] cookies = request.getCookies();
		String username = "";
		String password = "";
		
		if(cookies != null){
			for(Cookie ck : cookies){			
				if(ck.getName().equals(Constants.username)){
					username = ck.getValue();
				}
				else if(ck.getName().equals(Constants.password)){
					password = ck.getValue();
				}				
			}
		}
		if(!username.isEmpty() && !password.isEmpty()){
			account = new Account(username, password);
		}
		
		return account;
	}
	
	//TODO: add it to MenuItemsService class
	private Map<Integer,String> getMenuItemsMap(Restaurant restaurant){
		 List<MenuItem> menuItems = restaurant.getMenuItems();
		 
		Map<Integer,String> menuItemsMap = new HashMap<Integer, String>();
		
		for(MenuItem menuItem : menuItems){
			menuItemsMap.put(menuItem.getId(), menuItem.getName());
		}
		
		for(int key : menuItemsMap.keySet()){
			System.out.println(key + " : " + menuItemsMap.get(key));
		}
		
		return menuItemsMap;
	}
}
