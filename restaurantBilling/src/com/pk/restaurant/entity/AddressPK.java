package com.pk.restaurant.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class AddressPK implements Serializable {

	private static final long serialVersionUID = 3542876851667141528L;

	@Column(name="customer_id")
	private int customerId;
	
	@Column(name="address_ordinal")
	private int addressOrdinal;
	
	public AddressPK() {
		
	}

	public AddressPK(int customerId, int addressOrdinal) {
		this.customerId = customerId;
		this.addressOrdinal = addressOrdinal;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public int getAddressOrdinal() {
		return addressOrdinal;
	}

	public void setAddressOrdinal(int addressOrdinal) {
		this.addressOrdinal = addressOrdinal;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + addressOrdinal;
		result = prime * result + customerId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		
		AddressPK other = (AddressPK) obj;
		
		if (addressOrdinal != other.addressOrdinal)	return false;
		if (customerId != other.customerId)	return false;
		
		return true;
	}

	@Override
	public String toString() {
		return "AddressPK [customerId=" + customerId + ", addressOrdinal=" + addressOrdinal + "]";
	}
	
}
