package com.pk.restaurant.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="customer")
public class Customer implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5006297667697861479L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)	// Auto-Increment Id
	@Column(name="id")
	private int id;
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="middle_name")
	private String middleName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="ph_no", unique=true)
	private String phNo;
	
	@Column(name="email_id", unique=true)
	private String emailId;
	
	@Column(name="address_ordinal")
	private int addressOrdinal;		// will hold the next address ordinal number 
	
	@OneToMany(mappedBy = "customer", 
			cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, // "customer" -> "Address" class
			orphanRemoval = true,			// {L} when an address is removed from addresses set, it will be automatically removed from the DB also.
			fetch = FetchType.LAZY )		// {L} LAZY is the default FetchType for @OneToMany, if you don't specify it, it will take LAZY itself by default
	@JsonManagedReference
	private List<Address> addresses = new ArrayList<Address>();// Need to initialize it because of a bug in Hibernate: https://hibernate.atlassian.net/browse/HHH-9940
																// was giving error while fetching the customer's address from DB
	public Customer() {

	}

	public Customer(String firstName, String middleName, String lastName, String phNo, String emailId) {
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.phNo = phNo;
		this.emailId = emailId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhNo() {
		return phNo;
	}

	public void setPhNo(String phNo) {
		this.phNo = phNo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	public int getAddressOrdinal() {
		return addressOrdinal;
	}

	public void setAddressOrdinal(int addressOrdinal) {
		this.addressOrdinal = addressOrdinal;
	}

	public List<Address> getAddresses() {
		return addresses;
	}

	public void setAddresses(List<Address> addresses) {
		this.addresses = addresses;
	}
	
	/* 
	 * As Customer is not the owner of the "Customer --< Address" relationship.
	 * It can not make any update to the Address side. So to handle it this
	 * method is added.
	 */
	public void addAddress(Address address){
		if(addresses == null)
			addresses = new ArrayList<Address>();
		addresses.add(address);
		address.setCustomer(this);
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", firstName=" + firstName + ", middleName=" + middleName + ", lastName="
				+ lastName + ", phNo=" + phNo + ", emailId=" + emailId + ", addressOrdinal=" + addressOrdinal + "]";
	}

//	@Override
//	public String toString() {
//		return "Customer [id=" + id + ", firstName=" + firstName + ", middleName=" + middleName + ", lastName="
//				+ lastName + ", phoNo=" + phoNo + ", emailId=" + emailId + ", addresses=" + addresses + "]";
//	}
	
}
