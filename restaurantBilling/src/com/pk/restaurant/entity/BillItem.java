package com.pk.restaurant.entity;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="bill_items")
public class BillItem {

	@EmbeddedId
	private BillItemPK primaryKey = new BillItemPK();// composite primary key
	
	@Column(name="quantity")
	private int quantity;
	
	@Column(name="price")
	private Float price;

	public BillItemPK getPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(BillItemPK primaryKey) {
		this.primaryKey = primaryKey;
	}
	
	@Transient // {L} so that Hibernate doesn�t try to map these getters
	public Bill getBill() {
		return primaryKey.getBill();
	}

	public void setBill(Bill bill) {
		this.primaryKey.setBill(bill);
	}

	@Transient
	public MenuItem getMenuItem() {
		return primaryKey.getMenuItem();
	}

	public void setMenuItem(MenuItem menuItem) {
		this.primaryKey.setMenuItem(menuItem);
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Billitem [primaryKey=" + primaryKey + ", quantity=" + quantity + ", price=" + price + "]";
	}
}