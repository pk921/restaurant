package com.pk.restaurant.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "bill_address")
public class BillAddress {

	@Id
	@Column(name="bill_no")
	private Integer billNo;
	
	@Column(name="address")
	private String address;

	public BillAddress(){}
	
	public BillAddress(String address) {
		this.address = address;
	}

	public Integer getBillNo() {
		return billNo;
	}

	public void setBillNo(Integer billNo) {
		this.billNo = billNo;
	}
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "BillAddress [billNo=" + billNo + ", address=" + address + "]";
	}	
	
}
