package com.pk.restaurant.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name="address")
public class Address implements Serializable {

	private static final long serialVersionUID = 6278937170916497159L;

	@EmbeddedId						// Composite Primary key
	private AddressPK addressPK;
	
	@ManyToOne(cascade = {CascadeType.PERSIST}, fetch = FetchType.EAGER)	// {L} EAGER is the default FetchType for @ManyToOne
	@MapsId("customerId")			// {L} Maps the "customer_id" attribute of @EmbeddedId i.e. AddressPK
	@JsonBackReference
	private Customer customer;	    // used when there is "a Composite Primary Key which contains a @ManyToOne attribute as an @EmbeddedId"
	
	@Column(name="is_primary")
	private boolean isPrimary;
	
	@Column(name="address_line_1")
	private String addressLine1;
	
	@Column(name="address_line_2")
	private String addressLine2;
	
	@Column(name="landmark")
	private String landmark;
	
	@Column(name="city")
	private String city;
	
	@Column(name="state")
	private String state;
	
	@Column(name="country")
	private String country;
	
	@Column(name="postal_code")
	private String postalCode;
	
	@Column(name="ph_no")
	private String phNo;
	
	public Address() {
		
	}

	public Address(boolean isPrimary, String addressLine1, String addressLine2, String landmark, String city,
			String state, String country, String postalCode, String phNo) {
		this.isPrimary = isPrimary;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.landmark = landmark;
		this.city = city;
		this.state = state;
		this.country = country;
		this.postalCode = postalCode;
		this.phNo = phNo;
	}

	public Address(Customer customer, int addressOrdinal, boolean isPrimary, String addressLine1, String addressLine2,
			String landmark, String city, String state, String country, String postalCode) {
		this.addressPK = new AddressPK();
		// this.addressPK.setCustomerId(customer.getId()); // {L} Not required, will be automatically handled by "this.customer = customer;"
		this.addressPK.setAddressOrdinal(addressOrdinal);
		
		this.customer = customer;
		this.isPrimary = isPrimary;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.landmark = landmark;
		this.city = city;
		this.state = state;
		this.country = country;
		this.postalCode = postalCode;
	}

	public AddressPK getAddressPK() {
		return addressPK;
	}

	public void setAddressPK(AddressPK addressPK) {
		this.addressPK = addressPK;
	}
	
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public boolean isPrimary() {
		return isPrimary;
	}
	
	// Is added as JSTL was not picking "isPrimary()" 
	public boolean getIsPrimary() {
		return isPrimary;
	}

	public void setPrimary(boolean isPrimary) {
		this.isPrimary = isPrimary;
	}
	
	// Is added as JSTL was not picking "setPrimary(boolean isPrimary)"
	public void setIsPrimary(boolean isPrimary) {
		this.isPrimary = isPrimary;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getLandmark() {
		return landmark;
	}

	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	
	public String getPhNo() {
		return phNo;
	}

	public void setPhNo(String phNo) {
		this.phNo = phNo;
	}

	// {L} hashCode() & equals() method are used for comparing two objects of primary key object in the cache
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((addressPK == null) ? 0 : addressPK.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		
		Address other = (Address) obj;
		if (addressPK == null) {
			if (other.addressPK != null) return false;
		} else if (!addressPK.equals(other.addressPK))
			return false;
		
		return true;
	}

//	@Override
//	public String toString() {
//		return "Address [addressPK=" + addressPK + ", isPrimary=" + isPrimary + ", addressLine1=" + addressLine1
//				+ ", addressLine2=" + addressLine2 + ", landmark=" + landmark + ", city=" + city + ", state=" + state
//				+ ", country=" + country + ", postalCode=" + postalCode + "]";
//	}

	@Override
	public String toString() {
		return "Address [addressPK=" + addressPK + ", customer=" + customer + ", isPrimary=" + isPrimary
				+ ", addressLine1=" + addressLine1 + ", addressLine2=" + addressLine2 + ", landmark=" + landmark
				+ ", city=" + city + ", state=" + state + ", country=" + country + ", postalCode=" + postalCode
				+ ", phNo=" + phNo + "]";
	}
	
}
