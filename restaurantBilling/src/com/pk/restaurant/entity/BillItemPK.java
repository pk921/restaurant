package com.pk.restaurant.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class BillItemPK implements Serializable {

	private static final long serialVersionUID = 4099054307180299002L;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="bill_no")
	private Bill bill;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name="item_id")
	private MenuItem menuItem;

	public Bill getBill() {
		return bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}

	public MenuItem getMenuItem() {
		return menuItem;
	}

	public void setMenuItem(MenuItem menuItem) {
		this.menuItem = menuItem;
	}

	@Override
	public String toString() {
		if(bill == null){
			return "menuItem=" + menuItem.getId() + "]";
		}else 
			return "BillitemsPK [bill=" + bill.getBillNo() + ", menuItem=" + menuItem.getId() + "]";
	}	
}
