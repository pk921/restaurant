package com.pk.restaurant.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="restaurant")
public class Restaurant implements Serializable {

	private static final long serialVersionUID = -3313170763254577782L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="ph_no_1")
	private String phNo1;

	@Column(name="ph_no_2")
	private String phNo2;
	
	@Column(name="email_id")
	private String emailId;
	
	@Embedded
	private RestaurantAddress address;
	
	@OneToMany(mappedBy = "restaurant",	cascade = CascadeType.ALL, orphanRemoval = true,
			fetch = FetchType.LAZY)
	private List<Employee> employees = new ArrayList<Employee>();
	
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(name = "restaurant_items",  
		joinColumns = { @JoinColumn(name="restaurant_id") },
		inverseJoinColumns = { @JoinColumn(name="item_id") })
	private List<MenuItem> menuItems = new ArrayList<MenuItem>();

	public Restaurant() {}
	
	public Restaurant(int id, String name, String phNo1, String phNo2, String emailId,
			RestaurantAddress restaurantAddress) {
		this.id = id;
		this.name = name;
		this.phNo1 = phNo1;
		this.phNo2 = phNo2;
		this.emailId = emailId;
		this.address = restaurantAddress;
	}
	
	public Restaurant(int id, String name, String phNo1, String phNo2, String emailId,
			RestaurantAddress restaurantAddress, List<Employee> employees, List<MenuItem> menuItems) {
		this.id = id;
		this.name = name;
		this.phNo1 = phNo1;
		this.phNo2 = phNo2;
		this.emailId = emailId;
		this.address = restaurantAddress;
		this.employees = employees;
		this.menuItems = menuItems;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhNo1() {
		return phNo1;
	}

	public void setPhNo1(String phNo1) {
		this.phNo1 = phNo1;
	}

	public String getPhNo2() {
		return phNo2;
	}

	public void setPhNo2(String phNo2) {
		this.phNo2 = phNo2;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	public RestaurantAddress getAddress() {
		return address;
	}

	public void setAddress(RestaurantAddress restaurantAddress) {
		this.address = restaurantAddress;
	}

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public List<MenuItem> getMenuItems() {
		return menuItems;
	}

	public void setMenuItems(List<MenuItem> menuItems) {
		this.menuItems = menuItems;
	}

	@Override
	public String toString() {
		return "Restaurant [id=" + id + ", name=" + name + ", phNo1=" + phNo1 + ", phNo2=" + phNo2 + ", emailId="
				+ emailId + ", restaurantAddress=" + address + "]";
	}
}
