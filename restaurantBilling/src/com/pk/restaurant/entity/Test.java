package com.pk.restaurant.entity;

import java.io.Serializable;

import javax.persistence.Entity;

public class Test implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7487173655782906578L;
	private int id;
	private String fname;
	private String mname;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFname() {
		return fname;
	}
	public void setFname(String fname) {
		this.fname = fname;
	}
	public String getMname() {
		return mname;
	}
	public void setMname(String mname) {
		this.mname = mname;
	}
	@Override
	public String toString() {
		return "Test [id=" + id + ", fname=" + fname + ", mname=" + mname + "]";
	}
}
