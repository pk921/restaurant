package com.pk.restaurant.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="bill")
public class Bill {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="bill_no")
	private int billNo;

	@ManyToOne()
	@JoinColumn(name="customer_id")
	private Customer customer = new Customer();
	
	@ManyToOne(optional=false)
	@JoinColumn(name="restaurant_id")
	private Restaurant restaurant = new Restaurant();
	
	@ManyToOne(optional=false)
	@JoinColumn(name="employee_id")
	private Employee employee = new Employee();
	
	@Column(name="date_time")
	private Date date = new Date();
	
	@Column(name="amount")
	private Float amount;
	
	@Column(name="discount")
	private Float discount;
	
	@OneToMany(mappedBy = "primaryKey.bill", cascade = CascadeType.ALL)
	private List<BillItem> billItems = new ArrayList<BillItem>();
	
	@OneToOne(cascade = CascadeType.ALL)
	@PrimaryKeyJoinColumn
	private BillAddress billAddress;

	public int getBillNo() {
		return billNo;
	}

	public void setBillNo(int billNo) {
		this.billNo = billNo;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Restaurant getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Float getAmount() {
		return amount;
	}

	public void setAmount(Float amount) {
		this.amount = amount;
	}

	public Float getDiscount() {
		return discount;
	}

	public void setDiscount(Float discount) {
		this.discount = discount;
	}

	public List<BillItem> getBillItems() {
		return billItems;
	}

/*	
	public void setBillItems(List<BillItem> billItems) {
		this.billItems = billItems;
	}
*/	
	
	public void addBillItem(BillItem billItem){
		this.billItems.add(billItem);
		billItem.setBill(this);		
	}
	
	public BillAddress getBillAddress() {
		return billAddress;
	}

	public void setBillAddress(BillAddress billAddress) {
		this.billAddress = billAddress;
	}

	@Override
	public String toString() {
		return "Bill [billNo=" + billNo + ", customer=" + customer + ", restaurant=" + restaurant.getId()
				+ ", employee=" + employee.getFirstName() + ", date=" + date + ", amount=" + amount + ", discount="
				+ discount + ", menuItems=" + billItems + "]";
	}
}