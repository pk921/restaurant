package com.pk.restaurant.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.pk.restaurant.entity.Restaurant;

@Repository
public class RestaurantDAOImpl implements RestaurantDAO {
	
	@PersistenceContext(unitName = "restaurant_bill_manager")
	EntityManager entityManager;

	@Override
	public Restaurant getRestaurant(int id) {
		return entityManager.find(Restaurant.class, id);
	}

	@Override
	public void removeRestaurant(int id) {
		Restaurant tempRestaurant = entityManager.getReference(Restaurant.class, id);
		entityManager.remove(tempRestaurant);
	}

}
