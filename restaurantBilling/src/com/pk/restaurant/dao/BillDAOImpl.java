package com.pk.restaurant.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.pk.restaurant.entity.Bill;
import com.pk.restaurant.entity.BillAddress;
import com.pk.restaurant.entity.BillItem;
import com.pk.restaurant.entity.Customer;

@Repository
public class BillDAOImpl implements BillDAO {
	
	@PersistenceContext(unitName = "restaurant_bill_manager")
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<Bill> getBills(Customer customer, Date date) {
		List<Bill> billList = null;
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Bill> criteriaQuery = criteriaBuilder.createQuery(Bill.class);
		
		// from
		Root<Bill> billRoot = criteriaQuery.from(Bill.class);
		Path<Date> datePath = billRoot.get("date");
		// where
		Predicate condition1 = criteriaBuilder.equal(billRoot.get("customer"), customer); //TODO: remove hard coded values
		Predicate condition2 = criteriaBuilder.greaterThanOrEqualTo(datePath, date); //TODO: remove hard coded values
		criteriaQuery.where(condition1, condition2);
		criteriaQuery.orderBy(criteriaBuilder.desc(datePath));
		
		Query query = entityManager.createQuery(criteriaQuery);
		billList = ((List<Bill>) query.getResultList());
				
		return billList;
	}

	@Override
	public Bill getBill(int billNo) {
		return entityManager.find(Bill.class, billNo);
	}

	@Override
	public Bill saveBill(Bill bill) {
		
		// "BillItem" is the owner side of the relationship, not the "Bill"
		if(bill.getBillItems() != null && !bill.getBillItems().isEmpty()){
			List<BillItem> billItems = bill.getBillItems();
			for(BillItem billItem  : billItems){
				billItem.setBill(bill);
			}
		}
		
		BillAddress billAddress = bill.getBillAddress();
		bill.setBillAddress(null);
		
		entityManager.persist(bill);
		
		// {L} if "billAddress" is specified, then update its billno 
		// (As "billNo" in "BillAddress" entity can't be automatically generated in JPA from foreign key 
		//  (But in Hibernate it can be done automatically)),
		// thus can't be persisted with "bill" directly
		if(billAddress != null){
			String trimmedAddress = billAddress.getAddress().trim();
			if(!"".equals(trimmedAddress)){
				billAddress.setBillNo(bill.getBillNo());
				billAddress.setAddress(trimmedAddress);
				//entityManager.persist(billAddress);
				bill.setBillAddress(billAddress);
			}			
		}
		return bill;
	}

}
