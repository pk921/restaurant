package com.pk.restaurant.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.pk.restaurant.entity.Customer;

@Repository
public class CustomerDAOImpl implements CustomerDAO {

	// inject Entity manager
	@PersistenceContext(unitName = "restaurant_bill_manager") // It will work even if you don't mention unitName, as there is only one persistence unit in persistence.xml
	private EntityManager entityManager;
	
	@Override
	public Customer addCustomer(Customer customer) {
		//EntityManager entityManager = entityManagerFactory.createEntityManager();
		//entityManager.getTransaction().begin();
		
		//for adding a new customer, Id should be set automatically.
		customer.setId(0);
				
		if(customer.getPhNo() != null && customer.getPhNo().isEmpty()){
			customer.setPhNo(null);
		}		
		if(customer.getEmailId() != null && customer.getEmailId().isEmpty()){
			customer.setEmailId(null);
		}
		
		entityManager.persist(customer);
		entityManager.flush(); 
		//entityManager.getTransaction().commit();
		return customer;
	}

	//tested
	@Override
	public Customer getCustomerById(int customerId) {
		return entityManager.find(Customer.class, customerId);
	}
	
	
	//tested
	@Override
	public Customer getCustomerByPhoneNo(String phNo) {
		/*
		TypedQuery<Customer> createQuery = entityManager.createQuery("SELECT c FROM Customer c WHERE c.phNo1 = :phNo", Customer.class);
		createQuery.setParameter("phNo", phNo);
		
		List<Customer> customerList = createQuery.getResultList();
		
		if(customerList != null) {
			return customerList.get(0);
		}
		
		return null;
		*/
		// JPA Criteria Query
		Customer customer = null;
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Customer> criteriaQuery = criteriaBuilder.createQuery(Customer.class);
		
		// from
		Root<Customer> customerRoot = criteriaQuery.from(Customer.class);
		
		//where
		Predicate condition = criteriaBuilder.equal(customerRoot.get("phNo"), phNo); //TODO: remove hard coded values
		criteriaQuery.where(condition);
		
		Query query = entityManager.createQuery(criteriaQuery);
		customer = (Customer) query.getSingleResult();
		
		return customer;
		
	}

	//tested
	@Override
	public Customer getCustomerByEmailId(String emailId){

		Customer customer = null;
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Customer> criteriaQuery = criteriaBuilder.createQuery(Customer.class);
		
		// from
		Root<Customer> customerRoot = criteriaQuery.from(Customer.class);
		// where
		Predicate condition = criteriaBuilder.equal(customerRoot.get("emailId"), emailId); //TODO: remove hard coded values
		criteriaQuery.where(condition);
		
		Query query = entityManager.createQuery(criteriaQuery);
		customer = (Customer) query.getSingleResult();
		
		return customer;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<Customer> getAllCustomers() {
		List<Customer> customerList;
		/*
		Query query = entityManager.createQuery("from Customer", Customer.class);
		customerList = query.getResultList();
		*/
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Customer> criteriaQuery = criteriaBuilder.createQuery(Customer.class);
		//from
		Root<Customer> customerRoot = criteriaQuery.from(Customer.class);
		//select
		criteriaQuery.select(customerRoot);
		
		Query query = entityManager.createQuery(criteriaQuery);
		customerList = query.getResultList();
		return customerList;
	}

	//tested
	@Override
	public Customer updateCustomer(Customer customer) {
		Customer tempCustomer = getCustomerById(customer.getId());
		if(tempCustomer == null){
			return null;
		}
		// As we are not updating the Addresses, so we are using the same Addresses as in the DB
		customer.setAddresses(tempCustomer.getAddresses());
		customer = entityManager.merge(customer);
		return customer;
	}

	//tested
	@Override
	public void deleteCustomer(Customer customer) {		
		Customer tempCustomer = entityManager.getReference(Customer.class, customer.getId());
		entityManager.remove(tempCustomer);
	}

}
