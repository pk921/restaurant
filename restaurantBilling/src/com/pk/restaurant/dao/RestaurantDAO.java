package com.pk.restaurant.dao;

import com.pk.restaurant.entity.Restaurant;

public interface RestaurantDAO {

	// get restaurant details
	Restaurant getRestaurant(int id);
	
	void removeRestaurant(int id);
}
