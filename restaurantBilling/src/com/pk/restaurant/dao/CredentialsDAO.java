package com.pk.restaurant.dao;

import com.pk.restaurant.entity.Credentials;

public interface CredentialsDAO {

	Credentials checkCredential(String username, String password);
}
