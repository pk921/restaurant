package com.pk.restaurant.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.pk.restaurant.entity.Credentials;

@Repository
public class CredentialsDAOImpl implements CredentialsDAO {

	@PersistenceContext(unitName = "restaurant_bill_manager")
	private EntityManager entityManager;
	
	@Override
	public Credentials checkCredential(String username, String password) {
		
		// JPA Criteria Query
		Credentials credentials = null;
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Credentials> criteriaQuery = criteriaBuilder.createQuery(Credentials.class);
		
		// from
		Root<Credentials> credentialsRoot = criteriaQuery.from(Credentials.class);
		
		//where
		Predicate p1 = criteriaBuilder.equal(credentialsRoot.get("username"), username); //TODO: remove hard coded values
		Predicate p2 = criteriaBuilder.equal(credentialsRoot.get("password"), password);
		criteriaQuery.where(p1, p2);
		
		Query query = entityManager.createQuery(criteriaQuery);
		credentials = (Credentials) query.getSingleResult();
		
		return credentials;
	}

}
