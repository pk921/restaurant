package com.pk.restaurant.dao;

import java.util.List;

import com.pk.restaurant.entity.Customer;

public interface CustomerDAO {

	// add customer
	public Customer addCustomer(Customer customer);

	// get customer
	public Customer getCustomerById(int customerId);

	public Customer getCustomerByPhoneNo(String phNo);

	public Customer getCustomerByEmailId(String emailId);

	// get customers
	public List<Customer> getAllCustomers();

	// update customer;
	/**
	 * This method will update the customer in DB(if customer exist), based on
	 * the customer object provided in the parameter. It will not make any
	 * update/deletion to the Addresses Field of the customer.
	 * 
	 * @param customer
	 * @return updated Customer
	 */
	public Customer updateCustomer(Customer customer);

	// delete customer();
	public void deleteCustomer(Customer customer);

}
