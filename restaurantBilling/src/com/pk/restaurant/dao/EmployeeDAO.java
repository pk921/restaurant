package com.pk.restaurant.dao;

import com.pk.restaurant.entity.Employee;

public interface EmployeeDAO {

	Employee getEmployee(int id);
	void removeEmployee(int id);
}
