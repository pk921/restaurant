package com.pk.restaurant.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.pk.restaurant.entity.Employee;

@Repository
public class EmployeeDAOImpl implements EmployeeDAO {
	
	@PersistenceContext(unitName = "restaurant_bill_manager")
	EntityManager entityManager;

	@Override
	public Employee getEmployee(int id) {
		return entityManager.find(Employee.class, id);
	}

	@Override
	public void removeEmployee(int id) {
		Employee tempEmployee = entityManager.getReference(Employee.class, id);
		entityManager.remove(tempEmployee);
	}

}
