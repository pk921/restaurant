package com.pk.restaurant.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.pk.restaurant.entity.Address;
import com.pk.restaurant.entity.AddressPK;
import com.pk.restaurant.entity.Customer;

@Repository
public class AddressDAOImpl implements AddressDAO{

	@PersistenceContext(unitName = "restaurant_bill_manager")
	private EntityManager entityManager;
	
	@Autowired
	private CustomerDAO customerDAO;
	
	@SuppressWarnings("unchecked")
	@Override	
	public List<Address> getCustomerAddressList(int customerId) {
		Customer customer = customerDAO.getCustomerById(customerId);
		
		List<Address> address = null;
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Address> criteriaQuery = criteriaBuilder.createQuery(Address.class);
		
		// from
		Root<Address> addressRoot = criteriaQuery.from(Address.class);
		// where
		Predicate condition = criteriaBuilder.equal(addressRoot.get("customer"), customer); //TODO: remove hard coded values
		criteriaQuery.where(condition);
		
		Query query = entityManager.createQuery(criteriaQuery);
		address = query.getResultList();
		
		return address;
	}
	
	@Override	
	public Address getAddress(Customer customer, int addressOrdinal) {	
		return getAddress(customer.getId(), addressOrdinal);
	}
	
	@Override	
	public Address getAddress(int customerId, int addressOrdinal) {
		AddressPK addressPK = new AddressPK(customerId, addressOrdinal);
		return getAddress(addressPK);
	}
	
	@Override
	public Address getAddress(Address address) {
		entityManager.refresh(address);
		Address temp = entityManager.find(Address.class, address.getAddressPK());
		return temp;
	}
	
	@Override
	public Address getAddress(AddressPK addressPK) {
		return entityManager.find(Address.class, addressPK);
	}
	
	@Override	
	public void addAddressToCustomer(Address address, Customer customer) {		
		addAddressToCustomer(address, customer.getId());
	}

	@Override	
	public void addAddressToCustomer(Address address, int customerId) {	
		Customer customer = entityManager.find(Customer.class, customerId);
		
		// {L} we need to set it, as "address" is the owner of the relationship
		address.setCustomer(customer);
		//update the AddressOrdinal
		address.getAddressPK().setAddressOrdinal(customer.getAddressOrdinal());
		
		// Increment AddressOrdinal in Customer 
		customer.setAddressOrdinal(customer.getAddressOrdinal() + 1);
		
		// If address is set as primary Address
		if(address.isPrimary()){
			for(Address tempAddress : customer.getAddresses()){
				if(tempAddress.isPrimary()){
					tempAddress.setPrimary(false);
					entityManager.merge(tempAddress);
					break;
				}
			}	
		} else {
			// If it is the first address
			if(customer.getAddresses().size() == 0)
				address.setPrimary(true);
		}
		
		customer.getAddresses().add(address);
	}
	
	//need to test
	@Override	
	public Address updateAddress(Address address) {
		
		Address addressDB = getAddress(address.getAddressPK());
		if(addressDB == null){
			return null;
		}
		
		// If a primary address is maliciously set as non primary(this action is not permitted)
		if(!address.isPrimary() && addressDB.isPrimary()){
			address.setPrimary(addressDB.isPrimary());			
		}
		// If address is set as primary address
		else if(address.isPrimary() && !addressDB.isPrimary()){
			
			// update old primary address to non-primary
			Customer customer = addressDB.getCustomer();
			for(Address tempAddress : customer.getAddresses()){
				if(tempAddress.isPrimary()){
					tempAddress.setPrimary(false);
					entityManager.merge(tempAddress);
					break;
				}
			}			
		}
		//we are not changing customer
		address.setCustomer(addressDB.getCustomer());
				
		entityManager.merge(address);
		return address;
	}

	@Override
	public Boolean removeAddress(AddressPK addressPK) {
		Address address = entityManager.find(Address.class, addressPK);
		
		if(address == null) return true;
		
		Customer customer = address.getCustomer();
		if(address.isPrimary()){		
			for(Address tempAddress : customer.getAddresses()){
				if(!tempAddress.equals(address)){
					tempAddress.setPrimary(true);
					entityManager.merge(tempAddress);
					break;
				}
			}
			
		}
		// {L} Here we are removing address by removing it from customer(not directly through em.remove()),
		// to maintain the consistency of address removal in the persistence context(for "customer" entity)
		return customer.getAddresses().remove(address); 
		//entityManager.remove(address);
	}
	
	@Override	
	public Boolean removeAddress(Address address) {
		return removeAddress(address.getAddressPK());
	}

	@Override
	public Boolean removeAddress(Customer customer, int addressOrdinal) {
		return removeAddress(customer.getId(), addressOrdinal);
	}
	
	@Override
	public Boolean removeAddress(int customerId, int addressOrdinal) {
		AddressPK addressPK = new AddressPK(customerId, addressOrdinal);
		return removeAddress(addressPK);
	}

}
