package com.pk.restaurant.dao;

import java.util.List;

import com.pk.restaurant.entity.Address;
import com.pk.restaurant.entity.AddressPK;
import com.pk.restaurant.entity.Customer;

public interface AddressDAO {

	// get Address
	List<Address> getCustomerAddressList(int customerId);
	
	Address getAddress(Address address);
	
	Address getAddress(AddressPK addressPK);

	Address getAddress(int customerId, int addressOrdinal);

	Address getAddress(Customer customer, int addressOrdinal);

	
	// Add Address to Customer
	/**
	 * This method will add address to the customer and contain logic for
	 * setting primary address. it will also increment the addressOrdinality of
	 * the customer.
	 * 
	 * @param address
	 * @param customerId
	 */
	void addAddressToCustomer(Address address, int customerId);

	/**
	 * This method will add address to the customer and contain logic for
	 * setting primary address. it will also increment the addressOrdinality of
	 * the customer.
	 * 
	 * @param address
	 * @param customer
	 */
	void addAddressToCustomer(Address address, Customer customer);
	
	
	// update Address
	Address updateAddress(Address address);

	// remove Address
	/**
	 * This method will remove the address based on the parameter. In case a
	 * primary address is being removed, the method will set the next address as
	 * the primary key.
	 * 
	 * @param addressPK
	 * @return true/false
	 */
	Boolean removeAddress(AddressPK addressPK);

	/**
	 * This method will remove the address based on the parameter. In case a
	 * primary address is being removed, the method will set the next address as
	 * the primary key.
	 * 
	 * @param address
	 * @return true/false
	 */
	Boolean removeAddress(Address address);

	/**
	 * This method will remove the address based on the parameters. In case a
	 * primary address is being removed, the method will set the next address as
	 * the primary key.
	 * 
	 * @param customerId
	 * @param addressOrdinal
	 * @return true/false
	 */
	Boolean removeAddress(int customerId, int addressOrdinal);

	/**
	 * This method will remove the address based on the parameters. In case a
	 * primary address is being removed, the method will set the next address as
	 * the primary key.
	 * 
	 * @param customer
	 * @param addressOrdinal
	 * @return true/false
	 */
	Boolean removeAddress(Customer customer, int addressOrdinal);

}
