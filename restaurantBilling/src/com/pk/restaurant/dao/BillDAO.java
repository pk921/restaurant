package com.pk.restaurant.dao;

import java.util.Date;
import java.util.List;

import com.pk.restaurant.entity.Bill;
import com.pk.restaurant.entity.Customer;

public interface BillDAO {

	Bill getBill(int billNo);
	List<Bill> getBills(Customer customer, Date date);
	Bill saveBill(Bill bill);
}
