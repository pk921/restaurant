package com.pk.restaurant.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.pk.restaurant.entity.Employee;

public class AuthenticationInterceptor implements HandlerInterceptor {

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse resonse, Object handler, Exception arg3)
			throws Exception {
		//System.out.println((">>>> AfterCompletion"));
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView arg3)
			throws Exception {
		//System.out.println((">>>> postHandle"));
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		
		//System.out.println((">>>> preHandle"));
		System.out.println(request.getRequestURI());
		if(request.getRequestURI().equals("/restaurantBilling/account.html") ||
			request.getRequestURI().equals("/restaurantBilling/account/login.html")){
			return true;
		}
		
		HttpSession session = request.getSession();
		System.out.println(session.getId());
		Employee employee = (Employee) session.getAttribute("employee");
		if(employee == null){
			response.sendRedirect("/restaurantBilling/account/login.html");
			return false;
		}
		 
		return true;
	}

}
