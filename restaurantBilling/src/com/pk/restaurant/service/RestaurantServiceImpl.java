package com.pk.restaurant.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pk.restaurant.dao.RestaurantDAO;
import com.pk.restaurant.entity.Restaurant;

@Service
public class RestaurantServiceImpl implements RestaurantService {

	@Autowired
	RestaurantDAO restaurantDAO;
	
	@Transactional
	@Override
	public Restaurant getRestaurant(int id) {
		return restaurantDAO.getRestaurant(id);		
	}

	@Transactional
	@Override
	public void removeRestaurant(int id) {
		restaurantDAO.removeRestaurant(id);
	}

}
