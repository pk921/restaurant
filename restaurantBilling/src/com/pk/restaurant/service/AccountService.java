package com.pk.restaurant.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pk.restaurant.dao.CredentialsDAO;
import com.pk.restaurant.entity.Credentials;

@Service
public class AccountService {
	
	@Autowired
	private CredentialsDAO credentialsDAO;

	@Transactional
	public Credentials login(String username, String password){
		//return username.equalsIgnoreCase("admin") && password.equals("admin");
		Credentials credentials =  credentialsDAO.checkCredential(username, password);
		return credentials;
	}
}
