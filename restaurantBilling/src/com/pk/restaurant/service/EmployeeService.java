package com.pk.restaurant.service;

import com.pk.restaurant.entity.Employee;

public interface EmployeeService {

	Employee getEmployee(int id);
	void removeEmployee(int id);
}
