package com.pk.restaurant.service;

import com.pk.restaurant.entity.Restaurant;

public interface RestaurantService {

	Restaurant getRestaurant(int id);
	
	void removeRestaurant(int id);
}
