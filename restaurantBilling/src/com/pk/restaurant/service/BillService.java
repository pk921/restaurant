package com.pk.restaurant.service;

import java.util.Date;
import java.util.List;

import com.pk.restaurant.entity.Bill;
import com.pk.restaurant.entity.Customer;

public interface BillService {
	
	public List<Bill> getBills(Customer customer);
	
	public Bill getBill(int billNo);
	
	public List<Bill> getBill(Customer customer, Date date);
	
	public Bill saveBill(Bill bill);
}
