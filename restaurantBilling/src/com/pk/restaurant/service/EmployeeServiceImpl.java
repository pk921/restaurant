package com.pk.restaurant.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pk.restaurant.dao.EmployeeDAO;
import com.pk.restaurant.entity.Employee;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeDAO employeeDAO;
	
	@Transactional
	@Override
	public Employee getEmployee(int id) {
		return employeeDAO.getEmployee(id);
	}

	@Transactional
	@Override
	public void removeEmployee(int id) {
		employeeDAO.removeEmployee(id);
	}

}
