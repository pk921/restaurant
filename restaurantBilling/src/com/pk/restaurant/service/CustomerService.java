package com.pk.restaurant.service;

import com.pk.restaurant.entity.Customer;

public interface CustomerService {

	public Customer addCustomer(Customer customer);
	
	/**
	 * Get Customer by Customer Id
	 * @param customerId
	 * @return Customer object
	 */
	public Customer getCustomer(int customerId);
	
	/**
	 * Find Customer by CustomerID, If not found then finds by Phone number, If not found then finds by Email Id.
	 * @param customer
	 * @return Customer object
	 */
	public Customer getCustomer(Customer customer);
	
	public Customer updateCustomer(Customer customer);
	
	public void deleteCustomer(Customer customer);
	
	//public Customer removeAddress(Customer customer, int addressOrdinal);
}
