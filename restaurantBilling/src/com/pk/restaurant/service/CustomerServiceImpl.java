package com.pk.restaurant.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pk.restaurant.dao.CustomerDAO;
import com.pk.restaurant.entity.Customer;

@Service
public class CustomerServiceImpl implements CustomerService {
	
	@Autowired
	private CustomerDAO customerDAO;
		
	//tested
	@Override
	@Transactional
	public Customer addCustomer(Customer customer) {		
		return customerDAO.addCustomer(customer);
	}
	
	@Override
	@Transactional
	public Customer getCustomer(int customerId){
		if(customerId != 0){
			return customerDAO.getCustomerById(customerId);
		}
		return null;
	}

	//tested
	@Override
	@Transactional
	public Customer getCustomer(Customer customer) {
		Customer tempCustomer = null;
		
		// If no customer is provided
		if(customer == null) return null;
		
		// If customer id is provided
		tempCustomer = getCustomer(customer.getId());
		if(tempCustomer != null){
			customer = tempCustomer;
			return customer;
		}
		
		// Search for Phone number, if not found by id
		if(customer.getPhNo() != null && !customer.getPhNo().isEmpty()){
			
			// search in primary phone number list
			tempCustomer = customerDAO.getCustomerByPhoneNo(customer.getPhNo());
			if(tempCustomer != null){
				
				// In case customer is found via primary number, but email Id is not matching
				if(!customer.getEmailId().isEmpty()){
					if(!customer.getEmailId().equals(tempCustomer.getEmailId())){
						//TODO: display message "Provided email id (" + customer.getEmailId()
						// + ") doesn't match with the one in the database (" + tempCustomer.getEmailId() + ")"
					}
				}
				
				customer = tempCustomer;
				return customer;
			}
		}
			
		// Search for email, if not found by id or phone number
		if(customer.getEmailId() != null && !customer.getEmailId().isEmpty()){
			
			// search in email list
			tempCustomer = customerDAO.getCustomerByEmailId(customer.getEmailId());
			if(tempCustomer != null){
				
				// In case customer is found via email, but phone number is not matching
				if(!customer.getPhNo().isEmpty()){
					if(!customer.getPhNo().equals(tempCustomer.getPhNo())){
						// TODO: display message "Provided Primary Phone number (" + customer.getPhNo1()
						// + ") doesn't match with the one in the database (" + tempCustomer.getPhNo1() + ")"
					}
				}
				customer = tempCustomer;
				return customer;
			}
		}

		return null;
	}

	//tested
	@Override
	@Transactional
	public Customer updateCustomer(Customer customer) {
		return customerDAO.updateCustomer(customer);
	}

	//tested
	@Override
	@Transactional
	public void deleteCustomer(Customer customer) {
		customerDAO.deleteCustomer(customer);
		customer.setId(0);
	}

	/*
	//tested
	@Override
	@Transactional
	public Customer removeAddress(Customer customer, int addressOrdinal) {
		addressDAO.removeAddress(customer, addressOrdinal);
		return getCustomer(customer);
	}
	*/

}
