package com.pk.restaurant.service;

import java.util.List;

import com.pk.restaurant.entity.Address;
import com.pk.restaurant.entity.Customer;

public interface AddressService {

	List<Address> getCustomerAddressList(int customerId);
	
	public Address addAddress(Address address);
	
	public Address updateAddress(Address address);
	
	public Boolean removeAddress(Address address);
	
	public Boolean removeAddress(int customerId, int addressOrdinal);
	
	/** 
	 * @param customer
	 * @param addressOrdinal
	 * @return Customer Object, whose address is removed
	 */
	public Customer removeAddress(Customer customer, int addressOrdinal);

}
