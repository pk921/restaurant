package com.pk.restaurant.service;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pk.restaurant.dao.BillDAO;
import com.pk.restaurant.entity.Bill;
import com.pk.restaurant.entity.BillItem;
import com.pk.restaurant.entity.Customer;
import com.pk.restaurant.entity.MenuItem;

@Service
public class BillServiceImpl implements BillService {
	
	@Autowired
	private BillDAO billDAO;
	
	@Transactional
	public List<Bill> getBills(Customer customer){
		return null;
	}
	
	@Transactional
	public Bill getBill(int billNo){
		return billDAO.getBill(billNo);
	}
	
	@Transactional
	public List<Bill> getBill(Customer customer, Date date){		
		return billDAO.getBills(customer, date);
	}
	
	@Transactional
	public Bill saveBill(Bill bill){
		
		List<BillItem> billItems = bill.getBillItems();
		
		if(billItems == null || billItems.isEmpty())	return null;
		
		Iterator<BillItem> it= billItems.iterator();
		while(it.hasNext()){
			BillItem billItem = it.next();
			// If billItem number is 0, then remove that entry
			if(billItem.getMenuItem().getId() == 0){
				it.remove();
				continue;
			}
			// If price is set as zero, then pick the price from the MenuItem
			if(billItem.getPrice() == null || billItem.getPrice() == 0){
				for(MenuItem menuItem : bill.getRestaurant().getMenuItems()){
					if(menuItem.getId() == billItem.getMenuItem().getId()){
						billItem.setPrice(menuItem.getPrice());
						break;
					}
				}
			}
			// If quantity is set as zero, then update it to 1
			if(billItem.getQuantity() == 0){
				billItem.setQuantity(1);
			}
		}
		
		// If all the items get removed in the above loop
		if(billItems.isEmpty()) return null;
		
		bill.setBillNo(0);	//bill number will be generated automatically
		
		return billDAO.saveBill(bill);
	}

}
