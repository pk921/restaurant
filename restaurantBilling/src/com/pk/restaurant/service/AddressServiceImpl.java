package com.pk.restaurant.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pk.restaurant.dao.AddressDAO;
import com.pk.restaurant.entity.Address;
import com.pk.restaurant.entity.Customer;

@Service
public class AddressServiceImpl implements AddressService {
	
	@Autowired
	private AddressDAO addressDAO;
	
	@Autowired
	private CustomerService customerService;
	
	@Override
	public List<Address> getCustomerAddressList(int customerId){
		return addressDAO.getCustomerAddressList(customerId);
	}
	
	@Override
	@Transactional
	public Address addAddress(Address address) {
		
		if((address.getAddressLine1() != null && address.getAddressLine1().isEmpty()) &&
			(address.getAddressLine2() != null && address.getAddressLine2().isEmpty())){
			return null;
		}
		
		if((address.getCity() != null && address.getCity().isEmpty()) ||
			(address.getState() != null && address.getState().isEmpty())){
			return null;
		}
		
		if(address.getCustomer() != null)
			addressDAO.addAddressToCustomer(address, address.getCustomer());
		else if(address.getAddressPK() != null)
			addressDAO.addAddressToCustomer(address, address.getAddressPK().getCustomerId());
		
		return address;
	}

	@Override
	@Transactional
	public Address updateAddress(Address address) {
		return addressDAO.updateAddress(address);
	}

	@Override
	@Transactional
	public Boolean removeAddress(Address address) {
		return addressDAO.removeAddress(address);
	}

	@Override
	@Transactional
	public Boolean removeAddress(int customerId, int addressOrdinal) {
		return addressDAO.removeAddress(customerId, addressOrdinal);
	}
	
	@Override
	@Transactional
	public Customer removeAddress(Customer customer, int addressOrdinal) {
		addressDAO.removeAddress(customer, addressOrdinal);
		return customerService.getCustomer(customer);
	}

}
