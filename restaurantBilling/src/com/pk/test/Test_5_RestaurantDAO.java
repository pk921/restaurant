package com.pk.test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.pk.restaurant.entity.Restaurant;

public class Test_5_RestaurantDAO {
	
	public static void main(String[]  args){
	
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("restaurant_bill_manager");
		EntityManager em = emf.createEntityManager();
		EntityTransaction txn = em.getTransaction();
		
		try{
			// start transaction
			txn.begin();

			Restaurant restaurant = em.find(Restaurant.class, 1);
			System.out.println(restaurant);
			
			// commit transaction
			txn.commit();
			
		} catch (Exception e) {
			if(txn != null)
				txn.rollback();
			e.printStackTrace();
		} finally{
			if(em !=null)
				em.close();
			if(emf !=null)
				emf.close();
		}
		
	}
}
