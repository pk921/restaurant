package com.pk.test;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.pk.restaurant.entity.Address;
import com.pk.restaurant.entity.Customer;

public class Test_3_JPA_Customer {

	public static void main(String[] args) {
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("restaurant-pu");
		EntityManager em = emf.createEntityManager();
		EntityTransaction txn = em.getTransaction();
		
		try{
			// start transaction
			txn.begin();
			
			/* ******************************************************* * /
			// 1. Add New customer
			String firstName = "manoj";
			String lastName = "sharma";
			String phoNo1 = "3456789012";
			String phoNo2 = null;
			String emailId = "manoj@gmail.com";

			Customer customer = new Customer(firstName, lastName, phoNo1, phoNo2, emailId);
			em.persist(customer);
			/* */
			
			/* ******************************************************* * /
			// 2. Add New customer along with the Address
			String firstName = "manu";
			String middleName = null;
			String lastName = "gaur";
			String phoNo1 = "6565656778";
			String phoNo2 = null;
			String emailId = "sachin@gmail.com";
			
			int addressOrdinal = 1; 
			boolean isPrimary = true;
			String addressLine1 = "1234, moti nagar";
			String addressLine2 = "ludhiana, punjab";
			String addressLine3 = null;
			String postalCode = "141010";

			Customer customer = new Customer(firstName, middleName, lastName, phoNo1, phoNo2, emailId);
			System.out.println("1: " + customer);
			
			Address address = new Address(isPrimary, addressLine1, addressLine2, addressLine3, postalCode);
			address.getAddressPK().setAddressOrdinal(addressOrdinal);
			System.out.println("2: " + address);
			
			customer.addAddress(address);
			System.out.println("5: " + customer);
			em.persist(customer);
			System.out.println("6: " + customer);
			System.out.println("7: " + address);
			/* */
			
			/* ******************************************************* */
			// 3. Add New customer along with the Addresses
			// TODO: {L} You have to add customer to the address, otherwise it will through error "attempted to assign id from null one-to-one property"
			String firstName = "preet";
			String middleName = null;
			String lastName = "kapoor";
			String phoNo1 = "6565656778";
			String emailId = "gagan@gmail.com";
			
			int addressOrdinal = 1; 
			boolean isPrimary = true;
			String addressLine1 = "111, moti nagar";
			String addressLine2 = "ludhiana, punjab";
			String landmark = "";
			String city = "ludhiana";
			String state = "punjab";
			String country = "india";	
			String postalCode = "141010";
			
			List<Address> addresses = new ArrayList<Address>();
			Customer customer = new Customer(firstName, middleName, lastName, phoNo1, emailId);
			
			Address address = new Address(customer, addressOrdinal, isPrimary, addressLine1, addressLine2, landmark, city, state, country, postalCode);
			addresses.add(address);
			// /*OR* / customer.getAddresses().add(address);
			
			customer.setAddresses(addresses);
			em.persist(customer);
			/* */

			
			/* ******************************************************* * /
			// 4. read customer
			Customer customer = em.find(Customer.class, 1001);
			System.out.println(customer);
			
			// 5. delete customer <-- All the address corresponding to the customer will also get deleted --> 
			// em.remove(customer);
			/* */
			
			// commit transaction
			txn.commit();
			
		} catch (Exception e) {
			if(txn != null)
				txn.rollback();
			e.printStackTrace();
		} finally{
			if(em !=null)
				em.close();
			if(emf !=null)
				emf.close();
		}

	}

}
