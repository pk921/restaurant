package com.pk.test;

import com.pk.restaurant.dao.CustomerDAO;
import com.pk.restaurant.dao.CustomerDAOImpl;
import com.pk.restaurant.entity.Customer;

public class Test_4_CustomerDAO {

	public static void main(String[] args) {
		String firstName = "manu";
		String middleName = null;
		String lastName = "gaur";
		String phNo1 = "6565656778";
		String emailId = "sachin@gmail.com";
		
		Customer customer = new Customer(firstName, middleName, lastName, phNo1, emailId);
		System.out.println(customer);
		
		CustomerDAO customerDAO = new CustomerDAOImpl();		
		Customer cstmr = customerDAO.addCustomer(customer);
		
		System.out.println(cstmr);
		
	}

}
