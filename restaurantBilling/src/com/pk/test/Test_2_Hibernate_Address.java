package com.pk.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.pk.restaurant.entity.Address;
import com.pk.restaurant.entity.AddressPK;
import com.pk.restaurant.entity.Customer;

public class Test_2_Hibernate_Address {

	public static void main(String[] args) {

		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
								.addAnnotatedClass(Address.class)
								.addAnnotatedClass(Customer.class)
								.buildSessionFactory();

		Session session = factory.getCurrentSession();
		
		try{
		// start transaction
		session.beginTransaction();
		
		// read customer
		AddressPK addresspk = new AddressPK(1001,2);
		Address address = session.get(Address.class, addresspk);
		
		System.out.println(address);
		
		// commit transaction
		session.getTransaction().commit();
		
		
		} finally{
			factory.close();
		}

	}

}
