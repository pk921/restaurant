package com.pk.test;



import java.sql.DriverManager;

import java.sql.Connection;

public class Test_1_JDBC_Connection {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String jdbcUrl = "jdbc:mysql://localhost:3306/restaurant_bill_manager?useSSL=false";
		String user = "restaurant_admin";
		String password = "restaurant_pswrd";
		
		try{
			System.out.println("Connecting to the database: " + jdbcUrl);
			
			Connection myConn = DriverManager.getConnection(jdbcUrl, user, password);
			
			System.out.println("Connection successfull!!!");
			
		}
		catch (Exception exc){
			exc.printStackTrace();
		}

	}

}
