package com.pk.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.pk.restaurant.entity.Address;
import com.pk.restaurant.entity.Customer;

public class Test_2_Hibernate_Customer {

	public static void main(String[] args) {
		
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml")
								.addAnnotatedClass(Customer.class)
								.addAnnotatedClass(Address.class)
								.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try{
			// start transaction
			session.beginTransaction();
			
			// read customer
			Customer cstmr = session.get(Customer.class, 1001);
			
			System.out.println(cstmr);
			
			// commit transaction
			session.getTransaction().commit();
			
			
		} finally{
			factory.close();
		}

	}

}
