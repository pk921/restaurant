package com.pk.test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.pk.restaurant.entity.Address;
import com.pk.restaurant.entity.Customer;

public class Test_3_JPA_Address {

	public static void main(String[] args) {
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("restaurant-pu");
		EntityManager em = emf.createEntityManager();
		EntityTransaction txn = em.getTransaction();
		
		try{
			// start transaction
			txn.begin();
			
			/* ******************************************************* * /
			// 1. Adding new Address (Customer already exists)
			int customerId = 1005;
			int addressOrdinal = 3; 
			boolean isPrimary = false;
			String addressLine1 = "1234, focal point";
			String addressLine2 = "ludhiana, punjab";
			String addressLine3 = null;
			String postalCode = "141010";

			Customer customer = em.find(Customer.class, customerId);
			Address address = new Address(customer, addressOrdinal, isPrimary, addressLine1, addressLine2, addressLine3, postalCode);
				
			System.out.println(address);
			System.out.println(address.getCustomer());
					
			em.persist(address);
			
			System.out.println(address);
			System.out.println(address.getCustomer());
			
			/* */
			
			/* ******************************************************* */
			// 2. persisting new Customer from new Address (New address is added with new customer)
			String firstName = "Navneet";
			String middleName = "Singh";
			String lastName = "";
			String phoNo1 = "6565667718";
			String emailId = "navneet@gmail.com";
			
			int addressOrdinal = 2; 
			boolean isPrimary = true;
			String addressLine1 = "11, bhamia";
			String addressLine2 = "";	
			String landmark = "";
			String city = "ludhiana";
			String state = "punjab";
			String country = "india";
			String postalCode = "141010";	

			Customer customer = new Customer(firstName, middleName, lastName, phoNo1, emailId);
			Address address = new Address(customer, addressOrdinal, isPrimary, addressLine1, addressLine2, landmark, city, state, country, postalCode);
			
			System.out.println(customer);
			System.out.println(address);
			System.out.println(address.getCustomer());
			
			//em.persist(customer);
			em.persist(address);
			
			System.out.println(customer);
			System.out.println(address);
			System.out.println(address.getCustomer());
			
			/* */
			
			/* ******************************************************* * /
			// 3. read customer
			AddressPK addresspk = new AddressPK(1005,1);
			Address address = em.find(Address.class, addresspk);
			System.out.println(address);
			 
			// 4. delete address <-- Only the address will be delete, not the corresponding customer -->
			// em.remove(address);
			/* */
			
			// commit transaction
			txn.commit();
			
		} catch (Exception e) {
			if(txn != null)
				txn.rollback();
			e.printStackTrace();
		} finally{
			if(em !=null)
				em.close();
			if(emf !=null)
				emf.close();
		}

	}

}
