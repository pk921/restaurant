<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Restaurant</title>
</head>
<body>
	<!-- Header -->
	<jsp:include page="header.jsp"></jsp:include>
	<hr>
	
	<!-- Middle -->
	<div>
		<table style="width:100%;"> <%-- border="1"> --%>
			<tr>
				<!-- Left -->
				<td height="700" width="15%" style="border-right: 1px solid; vertical-align:top">
					<jsp:include page="leftmenu.jsp"></jsp:include>
				</td>	
				
				<!-- Right -->
				<td width="85%" style="vertical-align:top">
				<jsp:include page="${page}.jsp"></jsp:include>
					<div id="newbill"></div>
					<div id="search_customer"></div>
					<div id="search_bill"></div>
				</td>
			</tr>
		</table>
	</div>
	
	<!-- Footer -->
	<hr>
	<jsp:include page="footer.jsp"></jsp:include>
	
</body>
</html>