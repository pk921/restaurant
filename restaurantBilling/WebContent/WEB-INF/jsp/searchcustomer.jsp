<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html> 

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Restaurant</title>
</head>

<body>
<fieldset>
<legend>Customer Details</legend>
	<form:form method="post" modelAttribute="customerAttribute">
		<span style="color:red;">${error}</span>
		<span style="color:green;">${success}</span>
		<table style="width:95%">
			<tr>
				<td width = "15%"><b>Customer Id </b></td>
				<td width = "25%"><form:input path="id" readonly="true" cssStyle="width:100%" /></td>
			</tr>
			<tr>
				<td width = "15%"><b>Name </b></td>
				<td width = "25%"><form:input path="firstName" cssStyle="width:100%" /></td>
				<td width = "25%"><form:input path="middleName" cssStyle="width:100%" /></td>
				<td width = "25%"><form:input path="lastName" cssStyle="width:100%" /></td>				
			</tr>
			<tr>
				<td width = "15%"><b>Phone Number </b></td>
				<td width = "25%"><form:input path="phNo" maxlength="10" cssStyle="width:100%" /></td>
			</tr>
			<tr>
				<td width = "15%"><b>Email </b></td>
				<td width = "25%"><form:input path="emailId" cssStyle="width:100%" /></td>
			</tr>
			
			<tr>
				<td width = "15%"> &nbsp; </td>
				<td align="center"><input type="submit" name="update" value="Update" formaction="${pageContext.request.contextPath}/customer/updatecustomer"/>
					<input type="submit" name="delete" value="Delete" formaction="${pageContext.request.contextPath}/customer/deletecustomer" />
					<button type="button" id="add_address_button" onClick="unhide('new_address'); disable('add_address_button')"> Add Address </button>
				</td>
				
			</tr>
		</table>
	</form:form>
	<%---------------------------------------- New Address ---------------------------------------------%>
	<div id="new_address" style="display: none">
	<hr>
	<fieldset>
		<legend>New Address</legend>
		<form:form method="post" modelAttribute="addressAttribute">
			<form:hidden path="addressPK.customerId" value="${customerAttribute.id}" />
			<table style="width:100%">
				<tr style="outline: thin solid;">
					<td width="20%"></td>
					<td width="70%">
						<table style="width:100%" style="border: thin solid">
							<tr>
								<td><b> Primary Address </b></td>
								<td><b>:</b><form:checkbox path="isPrimary" /></td> 
							</tr>
							<tr>
								<td width="20%" align="left"><b> Address Line 1 </b></td>
								<td width="70%"><b>:</b> <form:input path="addressLine1" cssStyle="width:80%" /> </td>
							</tr>
							<tr>
								<td width="20%" align="left"><b> Address Line 2 </b></td>
								<td width="70%"><b>:</b> <form:input path="addressLine2" cssStyle="width:80%" /> </td>
							</tr>
							<tr>
								<td width="20%" align="left"><b> Landmark </b></td>
								<td width="70%"><b>:</b> <form:input path="landmark" cssStyle="width:80%" /> </td>
							</tr>
							<tr>
								<td width="20%" align="left"><b> City </b></td>
								<td width="70%"><b>:</b> <form:input path="city" cssStyle="width:80%" /> </td>
							</tr>
							<tr>
								<td width="20%" align="left"><b> State </b></td>
								<td width="70%"><b>:</b> <form:input path="state" cssStyle="width:80%" /> </td>
							</tr>
							<tr>
								<td width="20%" align="left"><b> Country </b></td>
								<td width="70%"><b>:</b> <form:input path="country" cssStyle="width:80%" /> </td>
							</tr>
							<tr>
								<td width="20%" align="left"><b> Postal Code </b></td>
								<td width="70%"><b>:</b> <form:input path="postalCode" cssStyle="width:80%" /> </td>
							</tr>
							<tr>
								<td width="20%" align="left"><b> Phone Number</b></td>
								<td width="70%"><b>:</b> <form:input path="phNo" cssStyle="width:80%" /> </td>
							</tr>					
						</table>
					</td>
					<td width = "10%" align="center" style="border-left: 1px solid;"> 
						<button type="button" id="cancel_new_address" onClick="hide('new_address'); enable('add_address_button')"> cancel </button>
						<br><input type="submit" name="add" value="Add"  formaction="${pageContext.request.contextPath}/address/addaddress"/>
					</td>
				</tr>
			</table>
		</form:form>
		</fieldset>
	</div>
	<%------------------------------------ Display Addresses -------------------------------------------%>
	<div id="display_addresses" >
	<c:if test="${customerAttribute.addresses.size() != 0}" >
		<hr>
		<table style="width:100%">	
			<tr style="outline: thin solid" >
				<th> Primary </th>
				<th> Phone No. </th>
				<th> Address Line 1 </th>
				<th> Address Line 2 </th>
				<th> Landmark </th>
				<th> City </th>
				<th> State </th>
				<th> Country </th>
				<th> Pin </th>
				<th> &nbsp; </th>
			</tr>		
			
			<%--------------------------------- Address Loop ----------------------------------%>
			<c:forEach items="${customerAttribute.addresses}" var="address" varStatus="status" >
			<form:form method="post" modelAttribute="addressAttribute">
			
				<%-----------------------------Display Address-----------------------------%>
				<tr id="tr${status.index}1" style="outline: thin solid" >
					<td align="center">
						<c:choose>
							<c:when test="${address.isPrimary}">
								<input type="checkbox" name="remember" checked disabled />
							</c:when>
							<c:otherwise>
								<input type="checkbox" name="remember" disabled />
							</c:otherwise>
						</c:choose>
					</td>					
					
					<td> ${address.phNo} </td>							
					<td> ${address.addressLine1} </td>
					<td> ${address.addressLine2} </td>
					<td> ${address.landmark} </td>
					<td> ${address.city} </td>
					<td> ${address.state} </td>
					<td> ${address.country} </td>
					<td> ${address.postalCode} </td>
					
					<td width = "10%" align="center" style="border-left: 1px solid;"> 
						<button type="button" id="edit_address" onClick="unhide('tr${status.index}2'); hide('tr${status.index}1')"> Edit </button>
						<input type="submit" name="remove_address" value="Remove" formaction="${pageContext.request.contextPath}/address/removeaddress"/>
					</td>
				</tr>
				
				<%-------------------------------Edit Address -----------------------------%>
				<tr id="tr${status.index}2" style="outline: thin solid; display: none">
					<td colspan="2"></td>
					<td colspan="7">
						<table style="width:100%" style="border: thin solid">
							<form:hidden path="addressPK.customerId" value="${address.addressPK.customerId}" />
							<form:hidden path="addressPK.addressOrdinal" value="${address.addressPK.addressOrdinal}" />
							<tr>
								<td><b> Primary Address </b></td>
								<td><b>:</b>
									<c:choose>
										<c:when test="${address.isPrimary}">
											<form:checkbox path="isPrimary" checked="checked" />
										</c:when>
										<c:otherwise>
											<form:checkbox path="isPrimary" />
										</c:otherwise>
									</c:choose>									
								</td> 
							</tr>
							<tr>
								<td width="20%" align="left"><b> Address Line 1 </b></td>
								<td width="70%"><b>:</b>
									<form:input path="addressLine1" value="${address.addressLine1}" cssStyle="width:80%" /> 
								</td>
							</tr>
							<tr>
								<td width="20%" align="left"><b> Address Line 2 </b></td>
								<td width="70%"><b>:</b>
									<form:input path="addressLine2" value="${address.addressLine2}" cssStyle="width:80%" />
								</td>
							</tr>
							<tr>
								<td width="20%" align="left"><b> Landmark </b></td>
								<td width="70%"><b>:</b>
									<form:input path="landmark" value="${address.landmark}" cssStyle="width:80%" />
								</td>
							</tr>
							<tr>
								<td width="20%" align="left"><b> City </b></td>
								<td width="70%"><b>:</b>
									<form:input path="city" value="${address.city}" cssStyle="width:80%" />
								</td>
							</tr>
							<tr>
								<td width="20%" align="left"><b> State </b></td>
								<td width="70%"><b>:</b>
									<form:input path="state" value="${address.state}" cssStyle="width:80%" />
								</td>
							</tr>
							<tr>
								<td width="20%" align="left"><b> Country </b></td>
								<td width="70%"><b>:</b>
									<form:input path="country" value="${address.country}" cssStyle="width:80%" />
								</td>
							</tr>
							<tr>
								<td width="20%" align="left"><b> Postal Code </b></td>
								<td width="70%"><b>:</b>
									<form:input path="postalCode" value="${address.postalCode}" cssStyle="width:80%" />
								</td>
							</tr>
							<tr>
								<td width="20%" align="left"><b> Phone Number</b></td>
								<td width="70%"><b>:</b>
									<form:input path="phNo" value="${address.phNo}" cssStyle="width:80%" />
								</td>
							</tr>
						</table>				
					</td>
					<td width = "10%" align="center" style="border-left: 1px solid;"> 
						<button type="button" id="no_address_change" onClick="hide('tr${status.index}2'); unhide('tr${status.index}1')"> No Change </button>
						<br><input type="submit" name="action" value="Update"  formaction="${pageContext.request.contextPath}/address/updateaddress"/>
					</td>
				</tr>
							
			</form:form>
			</c:forEach>
		</table>
	</c:if>
	</div>
</fieldset>
<script type="text/javascript">
	function hide(id){
		document.getElementById(id).style.display='none';
	}
	
	function unhide(id){
		document.getElementById(id).style.display='';
	}
	
	function disable(id){
		document.getElementById(id).disabled=true;
	}
	
	function enable(id){
		document.getElementById(id).disabled=false;
	}
	
	function confirmAddressDelete(){
		var msg=confirm("Do you want to proceed");
		if(msg == true){
			document.getElementById('submitButton').action = "/customer/list";
			document.getElementById('submitButton').submit();
		}
	}	
</script>
</body>
</html>