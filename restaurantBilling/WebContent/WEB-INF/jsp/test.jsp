<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>test</title>
</head>
<body>
<h1>Test..Restaurant</h1>
<hr>
<form:form method="post" modelAttribute="restaurantAttribute" >
		<fieldset>
			<legend>Test Restaurant</legend>
			<span style="color:red;">${error}</span>
			<span style="color:green;">${success}</span>
			
			<table>
				<tr>
					<td>Id</td>
					<td><form:input path="id" /></td>
				</tr>
			</table>
			
			<input type="submit" name="test_restaurant" value="test_restaurant" formaction="${pageContext.request.contextPath}/test/restaurant"/>
		</fieldset>
</form:form>

<form:form method="post" modelAttribute="employeeAttribute" >
		<fieldset>
			<legend>Test Employee</legend>
			<span style="color:red;">${error}</span>
			<span style="color:green;">${success}</span>
			
			<table>
				<tr>
					<td>Id</td>
					<td><form:input path="id" /></td>
				</tr>
			</table>
			
			<input type="submit" name="test_employee" value="test_employee" formaction="${pageContext.request.contextPath}/test/employee"/>
		</fieldset>
</form:form>

</body>
</html>