<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Display Bill</title>
<style>

#bill_items {
    border: 1px solid;
    border-radius: 12px;
    width:60%; 
    border-collapse: collapse;
}
</style>
</head>
<body>

<fieldset>
<legend>Bill Details</legend>

	<%---- Restaurant/Employee Details ----%>
	<div align="right">
		<table>
			<tr>
				<td><b> Restaurant </b></td>
				<td><b>:</b> ${billAttribute.restaurant.name} (#${billAttribute.restaurant.id})</td>
			</tr>
			<tr>
				<td><b> Employee </b></td>
				<td><b>:</b> ${billAttribute.employee.firstName} ${billAttribute.employee.middleName} ${billAttribute.employee.lastName} (#${billAttribute.employee.id})</td>
			</tr>
		</table>	
	</div>
	
	<%----------- Bill No. & Date -----------%>
	<b> &#35; ${billAttribute.billNo} </b> (${billAttribute.date})
	<hr>
	
	<%----------- Customer Details -----------%>
	<table id="customer_details">
		<tr>
			<td><b>Customer Id </b></td>
			<td><b>:</b> ${billAttribute.customer.id}</td>
		</tr>
		<tr>
			<td><b>Name </b></td>
			<td><b>:</b> 
				${billAttribute.customer.firstName} 
				${billAttribute.customer.middleName}
				${billAttribute.customer.lastName} 
			</td>				
		</tr>
		<tr>
			<td><b>Phone Number </b></td>
			<td><b>:</b> ${billAttribute.customer.phNo} </td>
		</tr>
		<tr>
			<td><b>Email </b></td>
			<td><b>:</b> ${billAttribute.customer.emailId} </td>
		</tr>
	</table>
	
	<%-------------- Bill Items --------------%>
	<table id="bill_items">
		<tr>
			<th>Id</th>
			<th align="left">Name</th>
			<th>Quantity</th>
			<th align="right">Price</th>			
		</tr>
		
		<c:set var="totalItems" value="${0}"/>
		<c:set var="totalAmount" value="${0}"/>
		<c:forEach items="${billAttribute.billItems}" var="billItem" >
		<tr align="center">
			<td>${billItem.primaryKey.menuItem.id}</td>
			<td align="left">${billItem.primaryKey.menuItem.name}</td>
			<td>${billItem.quantity}</td>
			<td align="right"> &#8377; ${billItem.price}</td>	
						
			<c:set var="totalItems" value="${totalItems + billItem.quantity}" />
			<c:set var="totalAmount" value="${totalAmount + billItem.price}" />
		</tr>	
		</c:forEach>
		
		<tr style="border-top: 1px solid; border-bottom: 1px solid;" align="center">
			<td></td>
			<td align="left"><b>Total:</b></td>
			<td>${totalItems}</td>
			<td align="right"> &#8377; ${totalAmount}</td>
		</tr>
		<tr align="right">
			<td colspan="3">Discount:</td>
			<td> &#8377; ${billAttribute.discount}</td>
		</tr>
		<tr align="right">
			<td colspan="3"><b>Net Amount:</b></td>
			<td><b> &#8377; ${billAttribute.amount}</b></td>
		</tr>
	</table>
</fieldset>
</body>
</html>
