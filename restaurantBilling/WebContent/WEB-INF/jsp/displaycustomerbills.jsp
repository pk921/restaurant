<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Search Bill By Customer</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>
<body>
<fieldset>
	<legend>Search Bill By Customer</legend>
	
	<%------------------- Customer Details -------------------%>
	<fieldset>
	<legend>Customer Details</legend>
		<span style="color:red;">${error}</span>
		<span style="color:green;">${success}</span>
		<form:form id="cust_detail_form" method="post" modelAttribute="customerAttribute" >
			<table>
				<tr>
					<td>Id</td>
					<td>: <form:input path="id" readonly="true" style="background-color: rgb(235, 235, 228)" /></td>
				</tr>
				<tr>
					<td>Name</td>
					<td>: <input id="name" type="text" value="${customerAttribute.firstName} ${customerAttribute.middleName} ${customerAttribute.lastName}" disabled /></td>			
				</tr>
				<tr>
					<td>Phone Number</td>
					<td>: <form:input path="phNo" disabled="true" /></td>
				</tr>
				<tr>
					<td>Email</td>
					<td>: <form:input path="emailId" disabled="true" /></td>
					<td>
						days:<input id="no_of_days" type="number" style="width: 3em;" />
						<button type="button" onclick="searchbills()">submit</button>
						<button type="button" onclick="resetCustDetailForm()" >Reset!</button>						
					</td>
				</tr>
			</table>
		</form:form>
	</fieldset>
	<%------------------------- Bills -------------------------%>
	<fieldset>
	<legend>Bills</legend>
		<table id="bills_table" style="width:100%; border: thin solid;  border-collapse: collapse;">
			<tr style="border: thin solid;">
				<th style="width=20%;"> Date/Time </th>
				<th style="width=10%;"> Bill Number </th>
				<th style="width=10%;"> Total Items </th>
				<th style="width=15%;"> Bill Amount </th>
				<th style="width=10%;"> Discount </th>
				<th style="width=15%;"> Net Amount </th>
				<th style="width=20%;"> &nbsp; </th>
			</tr>
			<c:forEach items="${billListAttribute}" var="bill" varStatus="loopStatus" >
				<tr id="bill_details_${loopStatus.index}" align="center" style="border: thin solid; border-bottom: none;">
					<td><b> ${bill.date} </b></td>
					<td><b><a href="${pageContext.request.contextPath}/bill/searchbill/byno/${bill.billNo}"> &#35; ${bill.billNo} </a></b></td>
					<td> ${fn:length(bill.billItems)} </td>
					<td> &#8377; ${bill.amount + bill.discount} </td>
					<td> &#8377; ${bill.discount} </td>
					<td> &#8377; ${bill.amount} </td>
					<td>
						<button type="button" id="show_button_${loopStatus.index}" onclick="showBillItems(${loopStatus.index})">Show Items</button>
						<button type="button" id="hide_button_${loopStatus.index}" onclick="hideBillItems(${loopStatus.index})" style="display:none">Hide Items</button>
					</td>
				</tr>
				<tr id="bill_items_${loopStatus.index}" align="center" style="border: thin solid; border-top: none; display:none">
					<td colspan="6">
						<table style="width:70%; margin:1em 0 1em 0; border: thin solid">
							<tr align="center">
								<th style="width:15%;"> Item Id </th>
								<th style="width:45%; padding-left:0.5em;" align="left"> Item Name </th>
								<th style="width:15%;"> Quantity </th>
								<th style="width:25%;" align="right"> Price </th>
							</tr>
							<c:forEach items="${bill.billItems}" var="billItem" >
								<tr align="center">
									<td>${billItem.primaryKey.menuItem.id}</td>
									<td style="padding-left:0.5em;" align="left">${billItem.primaryKey.menuItem.name}</td>
									<td>${billItem.quantity}</td>
									<td align="right"> &#8377; ${billItem.price}</td>	
								</tr>
							</c:forEach>
						</table>						
					</td>
					<td> &nbsp; </td>
				</tr>
			</c:forEach>
		</table>
	
	</fieldset>
</fieldset>
<script>

function showBillItems(index){
	$("#bill_items_"+index).show();
	$("#show_button_"+index).hide();
	$("#hide_button_"+index).show();
}
function hideBillItems(index){
	$("#bill_items_"+index).hide();
	$("#show_button_"+index).show();
	$("#hide_button_"+index).hide();
}

function searchbills(){
	// if no. of days entered is not valid, then return
	if(!validateNoOfDays()) return;

	// submit the form
	var action = "${pageContext.request.contextPath}/bill/searchbill/bycustomer/"+$("#no_of_days").val();
	$("#cust_detail_form").attr("action", action);
	$("#cust_detail_form").submit();
}

// no. of days should be between 0 & 366
function validateNoOfDays(){
	var days = $("#no_of_days").val();
	if(days<=0){
		alert("No. of days can't be zero or less");
		$("#no_of_days").focus();
		return false
	} else if(days > 365){
		alert("No. of days can't be more then 365");
		$("#no_of_days").focus();
		return false;
	}
	
	return true;
}

function resetCustDetailForm(){
	// clearing input values
	$("#cust_detail_form input").val("");	
	$("#no_of_days").val(1);
	$("#id").css("background-color", "");

	// enabling/disabling field
	$("#id").attr("readonly", false);
	$("#name").attr("disabled", true);
	$("#phNo").attr("disabled", false);
	$("#emailId").attr("disabled", false);
	
	// removing bills table
	$("#bills_table tr").remove();
}
</script>
</body>
</html>
