<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import =  "com.pk.restaurant.entity.Bill" %>
<%@ page import =  "java.util.*" %>
<!DOCTYPE html>
<% 
	@SuppressWarnings("unchecked")
	List<Bill> orderList = (List<Bill>) session.getAttribute("orderList");
	int orderCount  = 0;
	if(orderList != null)
		orderCount = orderList.size();	
%>
<!-- Left -->
<table>
	<tr><td><a href="${pageContext.request.contextPath}/bill/new">New Bill</a></td></tr>
	<tr>
		<td><a href="${pageContext.request.contextPath}/bill/order">Orders</a>
		<br>
		
		<ul>
		<% for(int i=0; i<orderCount; i++){ %>
			<li><a href="#" onclick="getOrder(<%=i%>)"><%=orderList.get(i).getBillNo()%></a></li>				
		<% } %>
		</ul>
	</td></tr>
	<tr><td><a href="${pageContext.request.contextPath}/customer/home.html">Search/Add Customer</a></td></tr>
	<tr><td><a href="${pageContext.request.contextPath}/bill/searchbill">Search Bill</a></td></tr>
	<tr><td><a href="${pageContext.request.contextPath}/menuitems/view.html">Menu Items</a></td></tr>
	<tr><td><a href="${pageContext.request.contextPath}/test.html">{Test}</a></td></tr>
</table>

<script>
function getOrder(index){
	// Redirecting to the url (to simulate someone clicking on a link)
	window.location.href = "${pageContext.request.contextPath}/bill/order/"+index;
}
</script>