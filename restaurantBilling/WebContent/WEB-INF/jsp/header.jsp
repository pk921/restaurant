<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>

<div>
	<table style="width:100%;"> <%-- border="1"> --%>
		<tr>
			<td width = "20%">ads </td>
			<td width = "60%" align="center"> <h1 style="margin:0">${sessionScope.restaurant.name}</h1> </td>
			<td width = "20%">
				<table>
					<tr>
						<td>User Id</td>
						<td>: ${sessionScope.employee.id}</td>
					</tr>
					<tr>
						<td>User Name</td>
						<td>: ${sessionScope.employee.firstName} ${sessionScope.employee.middleName} 
							  ${sessionScope.employee.lastName}
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr> 
			<td colspan="3" align="right">
				<a href="${pageContext.request.contextPath}/account/logout.html">Logout</a>
			</td> 
		</tr>
	</table>
</div>
