<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import =  "com.pk.restaurant.entity.Restaurant" %>
<%@ page import =  "com.pk.restaurant.entity.MenuItem" %>
<%@ page import =  "com.pk.restaurant.entity.Bill" %>
<%@ page import =  "com.pk.restaurant.entity.BillItem" %>
<%@ page import =  "java.util.*" %>

<!DOCTYPE html>
<html>
<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Generate Bill</title>
</head>

<body>
<% 
	Bill bill = (Bill) request.getAttribute("billAttribute");   
	List<BillItem> billItems = bill.getBillItems();
	int billItemsCount = billItems.size();
	int validBillItems = 0;
%>
	<span style="color:red;">${error}</span>
	<span style="color:green;">${success}</span>
	<datalist id ="itemIds"></datalist> 
	<datalist id ="itemNames"></datalist> 
	<datalist id = "customerAddresses"><option value=""/></datalist>
	
	<form:form id="billform" method="post" modelAttribute="billAttribute" onsubmit="return formValidation(this);">
	<form:input path="billNo" style="display:none"/>
	<%------------------------ Customer Details ----------------------%>
	
	<fieldset>
	<legend>Customer Details</legend>
	 		<span style="color:red;">${error_1}</span>
			<span style="color:green;">${success_1}</span>
			<table>
				<tr>
					<td>Id</td>
					<td><form:input path="customer.id" /></td>
				</tr>
				<tr>
					<td>Name</td>
					<td><form:input path="customer.firstName" /></td>
					<td><form:input path="customer.middleName" /></td>
					<td><form:input path="customer.lastName" /></td>			
				</tr>
				<tr>
					<td>Phone Number</td>
					<td><form:input path="customer.phNo" /></td>
					<td>Email</td>
					<td><form:input path="customer.emailId" /></td>
				</tr>
				<tr>
					<td>Address</td>
					<td colspan="2">
						<form:input path="billAddress.address" list="customerAddresses"  cssStyle="width:99%" />
					</td>
					<td>
						<button type="button" id="new_address" onclick="enableNewAddress()"> New Address? </button>
					</td>
				</tr>
				
				<tr id="newAddress" style="display: none">
					<td></td>
					<td colspan="3">
						<fieldset>
						<legend>New Address</legend>
						<table style="width:100%;">
							<tr>
								<td><b> Primary Address </b></td>
								<td><b>:</b> <input type="checkbox" id="isPrimary" /></td> 
							</tr>
							<tr>
								<td width="20%" align="left"><b> Address Line 1 </b></td>
								<td width="70%"><b>:</b> <input type="text" id="addressLine1" style="width:80%" /></td>
							</tr>
							<tr>
								<td width="20%" align="left"><b> Address Line 2 </b></td>
								<td width="70%"><b>:</b> <input type="text" id="addressLine2" style="width:80%" /> </td>
							</tr>
							<tr>
								<td width="20%" align="left"><b> Landmark </b></td>
								<td width="70%"><b>:</b> <input type="text" id="landmark" style="width:80%" /> </td>
							</tr>
							<tr>
								<td width="20%" align="left"><b> City </b></td>
								<td width="70%"><b>:</b> <input type="text" id="city" style="width:80%" /> </td>
							</tr>
							<tr>
								<td width="20%" align="left"><b> State </b></td>
								<td width="70%"><b>:</b> <input type="text" id="state" style="width:80%" /> </td>
							</tr>
							<tr>
								<td width="20%" align="left"><b> Country </b></td>
								<td width="70%"><b>:</b> <input type="text" id="country" style="width:80%" /> </td>
							</tr>
							<tr>
								<td width="20%" align="left"><b> Postal Code </b></td>
								<td width="70%"><b>:</b> <input type="text" id="postalCode" style="width:80%" /> </td>
							</tr>
							<tr>
								<td width="20%" align="left"><b> Phone Number</b></td>
								<td width="70%"><b>:</b> <input type="text" id="phNo" style="width:80%" /> </td>
							</tr>
							<tr>
								<td></td>
								<td>
									<button type="button" onclick="addAddress()"> Add Address </button>
									<button type="button" onclick="cancelNewAddress()"> Cancel </button>
									<button type="button" onclick="resetNewAddress()"> Reset! </button>
								</td>
							</tr>
						</table>
						</fieldset>
					</td>
				</tr>
				
				<tr id="customerButtons">
					<td>&nbsp;</td>
					<td colspan="3">
						<button type="button" onclick="searchcustomer()"> Search </button>
						<button type="button" onclick="addCustomer()">Add</button>
						<button type="button" onclick="resetCustomer()">Reset!</button>
					</td>
				</tr>
			</table>
	</fieldset>
 	
	<%-------------------------- Bill Details ------------------------%>
	<fieldset>
		<legend>Bill Items</legend>
		<div style="text-align:center"> <h2>Bill Items</h2> </div>
		<table style="text-align:center" >
			<tr>
			    <th>Id</th>
			    <th>Name</th>
			    <th>Quantity</th>
			    <th>Rate</th>
			    <th>Price</th>
			    <th>Action</th>
			</tr>
			
			<tr style="outline: thin solid red"> 
				<td> <input type="text" id="newItemId" list="itemIds" onblur="updateNewItemName()"> </td> 
				<td> <input type="text" id="newItemName" list="itemNames" onblur="updateNewItemId()"> </td>
				<td> <input type="number" id="newItemQuantity" onblur="updateNewItemPrice()"> </td>
				<td> <input type="number" id="newItemRate" readonly style="background-color: rgb(235, 235, 228)"> </td> 
				<td> <input type="number" id="newItemPrice"> </td>
				<td> 
					<button type="button" onclick="addItem()"> Add </button>
					<button type="button" onclick="resetNewItem()"> Reset! </button>
				</td>
			</tr>
			
			<%-- <c:forEach var = "i" begin = "0" end = "${sessionScope.restaurant.menuItems.size()-1}"> --%>
		<% 	int totalItems = ((Restaurant) session.getAttribute("restaurant")).getMenuItems().size();
			boolean display = true;
			for(int i=0; i<totalItems; i++) {
				//int i = (int) pageContext.getAttribute("i");
				display = true;
				pageContext.setAttribute("disabled", true);
				BillItem billItem;
				if(billItemsCount <= i){
					billItem = new BillItem();
					billItem.setMenuItem(new MenuItem());
				} else {
					billItem = billItems.get(i);
					if(billItem.getMenuItem().getId() != 0){
						validBillItems++;
						pageContext.setAttribute("disabled", false);
						display = false;
					}					
				}
		%>
			<%if(display) { %>
		 		<tr id="billItemDisplayRow_<%= i %>" style="display: none">
		 	<%} else{ %>
		 		<tr id="billItemDisplayRow_<%= i %>" >
		 	<%} %>
			 		<td class="itemId"><%= billItem.getMenuItem().getId() %></td>
				    <td class="itemName"><%= billItem.getMenuItem().getName() %></td>
				    <td class="itemQuantity"><%= billItem.getQuantity() %></td>
				    <td class="itemRate"></td>
				    <td class="itemPrice" align="right"><%= billItem.getPrice() %></td>
				    <td>
				    	<button type="button" id="edit_item_button<%= i %>" onclick="editCurrentItemRow(<%= i %>)"> Edit </button>
		         		<button type="button" id="delete_item_button<%= i %>" onclick="deleteCurrentItemRow(<%= i %>)"> Delete </button>
		         	</td>
			 	</tr> 
			
		 		<tr id="billItemFormRow_<%= i %>" style="display: none">
		 	<% pageContext.setAttribute("i", i); %>
			 	    <td><form:input class="formItemId" path="billItems[${i}].primaryKey.menuItem.id" readonly="true" disabled="${disabled}" style="background-color: rgb(235, 235, 228)"/></td>
				    <td><form:input class="formItemName" path="billItems[${i}].primaryKey.menuItem.name" readonly="true" disabled="${disabled}" style="background-color: rgb(235, 235, 228)" /></td>
				    <td><form:input class="formItemQuantity" path="billItems[${i}].quantity" disabled="${disabled}" onblur="updatePrice(this)" /></td>
				    <td><input type="number" class="formItemRate" readonly disabled style="background-color: rgb(235, 235, 228)" /></td>
				    <td><form:input class="formItemPrice" path="billItems[${i}].price" disabled="${disabled}"/></td>
				    <td>
				    	<button type="button" id="save_item_button<%= i %>" onclick="saveCurrentItemRow(<%= i %>)"> Save </button>
		         		<button type="button" id="delete_item_button<%= i %>" onclick="deleteCurrentItemRow(<%= i %>)"> Delete </button>
		         	</td>
				</tr> 
			<%--</c:forEach>--%>
		<%
			}
		%>
			
			<tr><td>&nbsp;</td></tr>
			<tr><td>&nbsp;</td></tr>
			<tr style="outline: thin solid black"></tr>
			<tr style="outline: thin solid black">
				<td></td>
				<td><b>Total:</b></td>
				<td id="totalItems"></td>
				<td id="totalAmountByRate"></td>
				<td id="totalAmount" align="right"></td>
			</tr>
			<tr align="right">
				<td colspan="4">Discount(&#8377;) :</td>
				<td><form:input path="discount" style="text-align:right;" onblur="updateBillAmount()"/></td>
			</tr>
			<tr align="right">
				<td colspan="4"><b>Net Amount(&#8377;):</b></td>
				<td><form:input path="amount" readonly="true" style="text-align:right; background-color: rgb(235, 235, 228)"/></td>
			</tr>
	    </table>
	</fieldset>
    <input type="submit" name="Save" value="Save" formaction="${pageContext.request.contextPath}/bill/order/save"/>
    <input type="submit" name="Delete" value="Delete" formaction="${pageContext.request.contextPath}/bill/order/delete"/>
    <input type="submit" name="Generate" value="Generate" formaction="${pageContext.request.contextPath}/bill/generate"/> 
	</form:form>

	<script type="text/javascript">
		var appContextPath = "${pageContext.request.contextPath}";
		var menuIdNameMap = {};
		var menuNameIdMap = {};
		var menuIdPriceMap = {};
		var maxBillItems = ${sessionScope.restaurant.menuItems.size()};
		var nextBillItemRow = <%= validBillItems %>;
		
		//tested
		$(document).ready(function() {
			options_itemIds = "";
			options_itemNames = "";	
			<%
				Restaurant restaurant = (Restaurant) session.getAttribute("restaurant");
				List<MenuItem> menuItems = restaurant.getMenuItems();
				for(MenuItem menuItem : menuItems){
			%>
					<%--menuIdList.push(<%=menuItem.getId()%>);--%> 
					<%--menuNameList.push("<%=menuItem.getName()%>");--%>
					menuIdNameMap[<%=menuItem.getId()%>] = "<%=menuItem.getName()%>";
					menuNameIdMap["<%=menuItem.getName()%>"] = <%=menuItem.getId()%>;
					menuIdPriceMap[<%=menuItem.getId()%>] = "<%=menuItem.getPrice()%>";
					
					options_itemIds += "<option value='<%=menuItem.getId()%>' />";
					options_itemNames += "<option value='<%=menuItem.getName()%>' />";		
			<%	
				}	
			%>
		
			document.getElementById('itemIds').innerHTML = options_itemIds;
			document.getElementById('itemNames').innerHTML = options_itemNames;
			getAddress();
			updateRate();
			calculateTotal();
		});
	</script>
	<script src="${pageContext.request.contextPath}/resources/js/generatebill.js"></script>
</body>
</html>