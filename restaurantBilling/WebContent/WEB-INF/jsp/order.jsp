<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import =  "com.pk.restaurant.entity.Bill" %>
<%@ page import =  "java.util.*" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Orders</title>
</head>
<body>
<fieldset>
<legend>Orders</legend>
	<table style="width:100%">
		<tr style="border-bottom: 1px solid;">
			<td><button type="button" onClick="openNewOrderPage()">New Order?</button></td>
		<tr>
		<tr>
			<td> Current Order #: <b>${orderNo}</b> </td>
		</tr>
		<tr>
			<td>
				<jsp:include page="generatebill.jsp"></jsp:include>
			</td>
		<tr>
	</table>
</fieldset>

<script>
function openNewOrderPage(){
	// Redirecting to the url (to simulate someone clicking on a link)
	window.location.href = "${pageContext.request.contextPath}/bill/order";
}
</script>
</body>
</html>