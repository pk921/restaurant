<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Restaurant Menu Items</title>
</head>
<body>
<fieldset>
<legend>Menu Items</legend>

<div align="center">
	<table style="width:95%" > <%-- border="1"> --%>
	
		<tr align="left">
			<th width="10%">Id</th>
			<th width="25%">Name</th>
			<th width="55%">Description</th>
			<th width="15%" align="right">Price</th>			
		</tr>
			
		<c:forEach items="${sessionScope.restaurant.menuItems}" var="menuItem" varStatus="status" >
		<tr align="left">
			<td>${menuItem.id}</td>
			<td>${menuItem.name}</td>
			<td>${menuItem.description}</td>
			<td align="right"> &#8377; ${menuItem.price}</td>			
		</tr>
	
		</c:forEach>
	</table>
</div>

</fieldset>
</body>
</html>