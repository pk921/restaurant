<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>

<div align="center">
&#9900; ${sessionScope.restaurant.address.address}, ${sessionScope.restaurant.address.city},
${sessionScope.restaurant.address.state}, ${sessionScope.restaurant.address.country} 
- ${sessionScope.restaurant.address.postalCode}     
<br>
&#9900; ${sessionScope.restaurant.phNo1}/${sessionScope.restaurant.phNo2}
&#9900; ${sessionScope.restaurant.emailId}
</div>