<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Restaurant</title>
</head>
<body>
	<form:form method="post" modelAttribute="customerAttribute" >
		<fieldset>
			<legend>Customer Details</legend>
			<span style="color:red;">${error}</span>
			<span style="color:green;">${success}</span>
			<table>
				<tr>
					<td>Id</td>
					<td><form:input path="id" /></td>
				</tr>
				<tr>
					<td>Name</td>
					<td><form:input path="firstName" /></td>
					<td><form:input path="middleName" /></td>
					<td><form:input path="lastName" /></td>				
				</tr>
				<tr>
					<td>Phone Number</td>
					<td><form:input path="phNo" /></td>
					<td>Email</td>
					<td><form:input path="emailId" /></td>
				</tr>
				
				<tr>
					<td>&nbsp;</td>
					<td colspan="3"><input type="submit" name="search" value="Search" formaction="${pageContext.request.contextPath}/customer/searchcustomer"/>
					<input type="submit" name="add" value="Add" formaction="${pageContext.request.contextPath}/customer/addcustomer"/>
					<input type="reset" value="Reset!">
					</td>
				</tr>
			</table>
		</fieldset>
	</form:form>

</body>
</html>
