<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<title>Search Bill</title>
</head>
<body>
	<br>
	<%----------------- Search by Bill No. -----------------%>
	<fieldset>
	<legend>Search Bill</legend>
		<table>
			<tr>
				<td> <b>Enter Bill No. :</b> </td>
				<td> <input type="number" id="billNoToSearch" /></td>
				<td> <button type="button" onclick="searchBillByNo()"> Search </button></td>
			</tr>
		</table>
	</fieldset>
	<br><br>
	<%----------------- Search by Customer -----------------%>
	<fieldset>
	<legend>Search Bill By Customer</legend>
	
		<table>
			<tr>
				<th> Customer Id </th>
				<th> Phone No. </th>
				<th> Email Id </th>
				<th> &nbsp; </th>
			</tr>
			<tr>
				<form:form id="cust_detail_form" method="post" modelAttribute="customerAttribute">
					<td><form:input path="id" /></td>
					<td><form:input path="phNo" /></td>
					<td><form:input path="emailId" /></td>
					<td>
						days:<input id="no_of_days" type="number" value="1" style="width: 3em;" />
						<button type="button" onclick="searchbills()">submit</button>
					</td>
				</form:form>
			</tr>
		</table>
	</fieldset>
	
	<script type="text/javascript">
	function searchBillByNo(){
		var billNo = $("#billNoToSearch").val();
		
		// Redirecting to the url (to simulate someone clicking on a link)
		window.location.href = "${pageContext.request.contextPath}/bill/searchbill/byno/"+billNo;
	}
	
	function searchbills(){
		if(!validateNoOfDays()) return;

		var action = "${pageContext.request.contextPath}/bill/searchbill/bycustomer/"+$("#no_of_days").val();
		$("#cust_detail_form").attr("action", action);
		$("#cust_detail_form").submit();
	}

	function validateNoOfDays(){
		var days = $("#no_of_days").val();
		if(days<=0){
			alert("No. of days can't be zero or less");
			$("#no_of_days").focus();
			return false
		} else if(days > 365){
			alert("No. of days can't be more then 365");
			$("#no_of_days").focus();
			return false;
		}
		
		return true;
	}
	</script>
	
</body>
</html>