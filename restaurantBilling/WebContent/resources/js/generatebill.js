
//tested
function enableNewAddress(){
	$("#newAddress").show();
	$("#customerButtons").hide();
}

//tested
function cancelNewAddress(){
	resetNewAddress();
	$("#newAddress").hide();
	$("#customerButtons").show();
}

//tested
function resetNewAddress(){
	$("#isPrimary").prop("checked", false);
	$("#addressLine1").val("");
	$("#addressLine2").val("");
	$("#landmark").val("");
	$("#city").val("");
	$("#state").val("");
	$("#country").val("");
	$("#postalCode").val("");
	$("#phNo").val("");
}

//tested
function resetCustomer(){
	$("#customer\\.id").val("");
	$("#customer\\.firstName").val("");
	$("#customer\\.middleName").val("");
	$("#customer\\.lastName").val("");
	$("#customer\\.phNo").val("");
	$("#customer\\.emailId").val("");	
	$("#billAddress\\.address").val("");
	
	cancelNewAddress();
}

//tested
function resetNewItem(){	
	$("#newItemId").val("");
	$("#newItemName").val("");
	$("#newItemQuantity").val("");
	$("#newItemRate").val("");
	$("#newItemPrice").val("");
	
	$("#newItemId").focus();
}

//tested
function searchcustomer(){
	var customer = {};
	customer["id"] = $("#customer\\.id").val();
	customer["phNo"] = $("#customer\\.phNo").val();
	customer["emailId"] = $("#customer\\.emailId").val();
	
	$.ajax({		
		type: "POST",
		data: JSON.stringify(customer),
		async: true,
		url: appContextPath + "/customer/bill/searchcustomer",
		dataType: "json",		// tells jquery what content type of response will receive.
		contentType: "application/json",	// the type of data you're sending. 
		success: function(response){
			$("#customer\\.id").val(response.id);
			$("#customer\\.firstName").val(response.firstName);
			$("#customer\\.middleName").val(response.middleName);
			$("#customer\\.lastName").val(response.lastName);
			$("#customer\\.phNo").val(response.phNo);
			$("#customer\\.emailId").val(response.emailId);
			
			$("#billAddress\\.address").val("");
			var options_addresses = "";
			for(var i=0; i<response.addresses.length; i++){

				var address = response.addresses[i];
				var addressText = createAddressText(address);
				
				options_addresses += '<option value="'+addressText+'" />';
			}
			$("#customerAddresses").html(options_addresses);
			//alert("Customer found!");
		},
		error: function(){
			alert("Customer not found!");
			return false;
		}
		
	});	
}

//tested
function addCustomer(){
	
	var customer = {};
	customer["id"] = $("#customer\\.id").val();
	customer["firstName"] = $("#customer\\.firstName").val();
	customer["middleName"] = $("#customer\\.middleName").val();
	customer["lastName"] = $("#customer\\.lastName").val();
	customer["phNo"] = $("#customer\\.phNo").val();
	customer["emailId"] = $("#customer\\.emailId").val();
	
	$.ajax({		
		type: "POST",
		data: JSON.stringify(customer),
		async: true,
		url: appContextPath + "/customer/bill/addcustomer",
		dataType: "json",		// tells jquery what content type of response will receive.
		contentType: "application/json",	// the type of data you're sending. 
		success: function(response){
			$("#customer\\.id").val(response.id);
			$("#customer\\.firstName").val(response.firstName);
			$("#customer\\.middleName").val(response.middleName);
			$("#customer\\.lastName").val(response.lastName);
			$("#customer\\.phNo").val(response.phNo);
			$("#customer\\.emailId").val(response.emailId);
			
			$("#billAddress\\.address").val("");
			var options_addresses = "";
			for(var i=0; i<response.addresses.length; i++){

				var address = response.addresses[i];
				var addressText = createAddressText(address);
				
				options_addresses += '<option value="'+addressText+'" />';
			}
			$("#customerAddresses").html(options_addresses);
			alert("Customer Created!");
		},
		error: function(){
			alert("Not able to create customer!");
			return false;
		}
		
	});	
}

function createAddressText(address){
	 var addressText= "";
		
	if(address.addressLine1 != null && $.trim(address.addressLine1) != "")
		addressText += address.addressLine1;
	if(address.addressLine2 != null && $.trim(address.addressLine2) != "")
		addressText = addressText + ", "+ address.addressLine2;
	if(address.landmark != null && $.trim(address.landmark) != "")
		addressText = addressText + ", "+ address.landmark;
	if(address.city != null && $.trim(address.city) != "")
		addressText = addressText + ", "+ address.city;
	if(address.state != null && $.trim(address.state) != "")
		addressText = addressText + ", "+ address.state;
	if(address.country != null && $.trim(address.country) != "")
		addressText = addressText + ", "+ address.country;
	if(address.postalCode != null && $.trim(address.postalCode) != "")
		addressText = addressText + ", "+ address.postalCode;
	if(address.phNo != null && $.trim(address.phNo) != "")
		addressText = addressText + ", "+ address.phNo;
	
	return addressText;
}

function getAddress(){
	var customerId = $("#customer\\.id").val();
	if(customerId <=1 ){
		return;
	}
	
	$.ajax({		
		type: "GET",
		async: true,
		url: appContextPath + "/address/bill/getcustomeraddresses/"+customerId,
		dataType: "json",		// tells jquery what content type of response will receive.
		contentType: "application/json",	// the type of data you're sending. 
		success: function(response){
			
			$("#billAddress\\.address").val("");
			var options_addresses = "";
			for(var i=0; i<response.length; i++){

				var address = response[i];
				var addressText = createAddressText(address);
				
				options_addresses += '<option value="'+addressText+'" />';
			}
			$("#customerAddresses").html(options_addresses);

		},
		error: function(){
			return false;
		}
		
	});	
}

function addAddress(){
	var custAddress = {};
	var addressPK = {};
	addressPK["customerId"] = $("#customer\\.id").val();
	custAddress["addressPK"] = addressPK;
	custAddress["isPrimary"] = $("#isPrimary").prop("checked");
	custAddress["addressLine1"] = $("#addressLine1").val();
	custAddress["addressLine2"] = $("#addressLine2").val();
	custAddress["landmark"] = $("#landmark").val();
	custAddress["city"] = $("#city").val();
	custAddress["state"] = $("#state").val();
	custAddress["country"] = $("#country").val();
	custAddress["postalCode"] = $("#postalCode").val();
	custAddress["phNo"] = $("#phNo").val();
		
	$.ajax({		
		type: "POST",
		data: JSON.stringify(custAddress),
		async: true,
		url: appContextPath + "/address/bill/addaddress",
		dataType: "json",		// tells jquery what content type of response will receive.
		contentType: "application/json",	// the type of data you're sending. 
		success: function(response){
			$("#billAddress\\.address").val("");
			var options_addresses = "";
			
				var address = response;
				var addressText = createAddressText(address);
				
				$("#billAddress\\.address").val(addressText);
				options_addresses += '<option value="'+addressText+'" />';
			
			$("#customerAddresses").html(options_addresses+$("#customerAddresses").html());
			
			alert("Address added!");
			$("#newAddress").hide();
		},
		error: function(){
			alert("Not able to add address!");
			return false;
		}
		
	});	
}

//tested
function editCurrentItemRow(row){
	$("#billItemDisplayRow_"+row).hide();
	$("#billItemFormRow_"+row).show();
}

//tested
function saveCurrentItemRow(row){	
	var billItemDisplayRow = $("#billItemDisplayRow_"+row);
	var billItemFormRow = $("#billItemFormRow_"+row);
	
	var price = formatAmount(billItemFormRow.find(".formItemPrice").val());
	billItemFormRow.find(".formItemPrice").val(price);

	billItemDisplayRow.find(".itemPrice").text(price);
	billItemDisplayRow.find(".itemQuantity").text(billItemFormRow.find(".formItemQuantity").val());
	
	billItemDisplayRow.show();
	billItemFormRow.hide();
	
	calculateTotal();
}

//tested
function formValidation(){
	// Bill Items Validation 
	// var maxBillItems = ${sessionScope.restaurant.menuItems.size()};	
	for(var i=0; i<maxBillItems; i++){
		var currBillItemFormRow = $("#billItemFormRow_"+i);
		var currBillItemDisplayRow = $("#billItemDisplayRow_"+i);
		if(currBillItemFormRow.find(".formItemId").attr("disabled") == "disabled"){
			break;
		}
		var menuId = currBillItemFormRow.find(".formItemId").val();
		var menuName = menuIdNameMap[menuId];
		if(menuName == undefined){
			currBillItemFormRow.find(".formItemId").val(menuId + " Is Not Correct!");
			currBillItemDisplayRow.find(".itemId").text(menuId + " Is Not Correct!");
			currBillItemDisplayRow.find(".itemId").css("color", "red");
			return false;
		}
	}
	return true;	
}

//tested
function addItem(){
	var itemId = $("#newItemId").val();
	var itemName = menuIdNameMap[itemId];
	// If item doesn't exist
	if(itemName == undefined){
		$("#newItemId").css("color", "red");
		$("#newItemName").css("color", "red");
		$("#newItemId").val("");
		$("#newItemName").val("Not Valid!");
		$("#newItemName").focus();
		return;
	}
	
	// If there is already a row present for the same menuItem
	var duplicateRow = checkDuplicateItemEntry();
	if( duplicateRow != -1){
		var billItemDisplayRow = $("#billItemDisplayRow_"+duplicateRow);
		var billItemFormRow = $("#billItemFormRow_"+duplicateRow);
		
		var oldQuantity = parseInt(billItemFormRow.find(".formItemQuantity").val());
		var oldPrice = parseFloat(billItemFormRow.find(".formItemPrice").val());
		
		var newQuantity = oldQuantity + parseInt($("#newItemQuantity").val());
		var newPrice = formatAmount(oldPrice + parseFloat($("#newItemPrice").val())); 
		
		billItemFormRow.find(".formItemQuantity").val(newQuantity);
		billItemFormRow.find(".formItemPrice").val(newPrice);
		
		billItemDisplayRow.find(".itemQuantity").text(newQuantity);
		billItemDisplayRow.find(".itemPrice").text(newPrice);
		
	} else{
		// If maximum bill item limit is reached
		if(nextBillItemRow >= maxBillItems){
			// no more rows can be added
			return;
		}
		var billItemDisplayRow = $("#billItemDisplayRow_"+nextBillItemRow);
		billItemDisplayRow.show();
		billItemDisplayRow.find(".itemId").text($("#newItemId").val());
		billItemDisplayRow.find(".itemName").text($("#newItemName").val());
		billItemDisplayRow.find(".itemQuantity").text($("#newItemQuantity").val());
		billItemDisplayRow.find(".itemRate").text($("#newItemRate").val());
		billItemDisplayRow.find(".itemPrice").text($("#newItemPrice").val());
		
		billItemFormRow = $("#billItemFormRow_"+nextBillItemRow);
		//billItemFormRow.show();
		billItemFormRow.hide();
		
		billItemFormRow.find(".formItemId").val($("#newItemId").val());
		billItemFormRow.find(".formItemName").val($("#newItemName").val());
		billItemFormRow.find(".formItemQuantity").val($("#newItemQuantity").val());
		billItemFormRow.find(".formItemRate").val($("#newItemRate").val());
		billItemFormRow.find(".formItemPrice").val($("#newItemPrice").val());
		
		billItemFormRow.find(".formItemId").removeAttr('disabled');
		billItemFormRow.find(".formItemName").removeAttr('disabled');
		billItemFormRow.find(".formItemQuantity").removeAttr('disabled');
		billItemFormRow.find(".formItemPrice").removeAttr('disabled');
		
		nextBillItemRow = nextBillItemRow + 1;
	}
	calculateTotal();
	resetNewItem();
}

//tested
//If duplicate entry is there, then return the row no., otherwise return -1
function checkDuplicateItemEntry(){	
	
	for(var i=0; i<maxBillItems; i++){
		var billItemFormRow = $("#billItemFormRow_"+i);
		
		if(billItemFormRow.find(".formItemId").attr("disabled") == "disabled") break;
		if(billItemFormRow.find(".formItemId").val() == $("#newItemId").val()){
			return i;
		}
	}
	return -1;
}

//tested
function deleteCurrentItemRow(row){
	
	var currBillItemDisplayRow = $("#billItemDisplayRow_"+row);
	var currBillItemFormRow = $("#billItemFormRow_"+row);
	currBillItemFormRow.hide();		
	currBillItemDisplayRow.show();
	
	var i;
	for(i=row; i<maxBillItems; i++){
		currBillItemDisplayRow = $("#billItemDisplayRow_"+i);
		currBillItemFormRow = $("#billItemFormRow_"+i);
		var nextBillItemFormRow = $("#billItemFormRow_"+(i+1));
		
		if(nextBillItemFormRow.find(".formItemId").attr("disabled") == "disabled"){
			break;
		}
		var itemId = nextBillItemFormRow.find(".formItemId").val();
		var itemName = nextBillItemFormRow.find(".formItemName").val();
		var itemQuantity = nextBillItemFormRow.find(".formItemQuantity").val();
		var itemRate = nextBillItemFormRow.find(".formItemRate").val();
		var itemPrice = nextBillItemFormRow.find(".formItemPrice").val();
		
		currBillItemFormRow.find(".formItemId").val(itemId);
		currBillItemFormRow.find(".formItemName").val(itemName);
		currBillItemFormRow.find(".formItemQuantity").val(itemQuantity);
		currBillItemFormRow.find(".formItemRate").val(itemRate);
		currBillItemFormRow.find(".formItemPrice").val(itemPrice);
			
		currBillItemDisplayRow.find(".itemId").text(itemId);
		currBillItemDisplayRow.find(".itemName").text(itemName);
		currBillItemDisplayRow.find(".itemQuantity").text(itemQuantity);
		currBillItemDisplayRow.find(".itemRate").text(itemRate);
		currBillItemDisplayRow.find(".itemPrice").text(itemPrice);

	}
	currBillItemFormRow.find(".formItemId").attr("disabled", "disabled");
	currBillItemFormRow.find(".formItemName").attr("disabled", "disabled");
	currBillItemFormRow.find(".formItemQuantity").attr("disabled", "disabled");
	currBillItemFormRow.find(".formItemPrice").attr("disabled", "disabled");
	
	currBillItemFormRow.hide();		
	currBillItemDisplayRow.hide();
	
	if(nextBillItemRow > 0)
		nextBillItemRow = nextBillItemRow - 1;
	
	calculateTotal();
}

//tested
function updateNewItemName(){	
	var itemId = $("#newItemId").val();
	var itemName = menuIdNameMap[itemId];
	
	if(itemName == undefined){
		$("#newItemId").css("color", "red");
		$("#newItemName").val("");
	}
	else{
		var itemPrice = formatAmount(menuIdPriceMap[itemId]);
		
		$("#newItemId").css("color", "black");	
		$("#newItemName").css("color", "black");
		$("#newItemName").val(itemName);
		$("#newItemQuantity").val(1);
		$("#newItemRate").val(itemPrice);
		$("#newItemPrice").val(itemPrice);
	}
}

//tested
function updateNewItemId(){
	var itemName = $("#newItemName").val();
	var itemId = menuNameIdMap[itemName];
	
	if(itemId == undefined){
		$("#newItemName").css("color", "red");
		$("#newItemId").val("");
	}
	else{
		var itemPrice = formatAmount(menuIdPriceMap[itemId]);
		$("#newItemName").css("color", "black");
		$("#newItemId").css("color", "black");
		$("#newItemId").val(itemId);
		$("#newItemQuantity").val(1);
		$("#newItemRate").val(itemPrice);
		$("#newItemPrice").val(itemPrice);
	}
}

//tested
function updateNewItemPrice(){
	var itemId = $("#newItemId").val();
	var quantity = $("#newItemQuantity").val();
	var itemPrice = menuIdPriceMap[itemId];
	
	if(quantity == 0){
		quantity = 1;
		$("#newItemQuantity").val(quantity);
		$("#newItemQuantity").css("color", "red");
	} else {
		$("#newItemQuantity").css("color", "black");
	}
	
	if(itemPrice == undefined){
		$("#newItemId").css("color", "red");
	} else{
		$("#newItemId").css("color", "black");
		$("#newItemPrice").val(formatAmount(quantity*itemPrice));		
	}
}

//tested
function updatePrice(quantityCol){
	var billItemFormRow = $(quantityCol).parent().parent();
	var itemId = billItemFormRow.find(".formItemId").val();
	var quantity = billItemFormRow.find(".formItemQuantity").val();
	var itemPrice = menuIdPriceMap[itemId];
	
	if(quantity == 0){
		quantity = 1;
		billItemFormRow.find(".formItemQuantity").val(quantity);
		billItemFormRow.find(".formItemQuantity").css("color", "red");
	} else {
		billItemFormRow.find(".formItemQuantity").css("color", "black");
	}
	
	if(itemPrice == undefined){
		billItemFormRow.find(".formItemId").css("color", "red");
	} else{
		billItemFormRow.find(".formItemId").css("color", "black");
		billItemFormRow.find(".formItemPrice").val(formatAmount(quantity*itemPrice));		
	}
}

//tested
function calculateTotal(){
	var totalItems = 0;
	var totalAmountByRate = 0;
	var totalAmount = 0;
	
	for(var i=0; i<maxBillItems; i++){
		var billItem = $("#billItemFormRow_"+i);
		if(billItem.find(".formItemId").attr("disabled") == "disabled"){
			break;
		}
		totalItems += parseInt(billItem.find(".formItemQuantity").val());
		totalAmountByRate += parseInt(billItem.find(".formItemQuantity").val()) * parseInt(billItem.find(".formItemRate").val());
		totalAmount += parseFloat(billItem.find(".formItemPrice").val());
	}
	$("#totalItems").text(totalItems);
	$("#totalAmountByRate").text(formatAmount(totalAmountByRate));
	$("#totalAmount").text(formatAmount(totalAmount));
	
	var discount = $("#discount").val();
	var netAmount = formatAmount(totalAmount - discount);
	$("#amount").val(netAmount);
}

function updateRate(){
	for(var i=0; i<maxBillItems; i++){
		var billItemFormRow = $("#billItemFormRow_"+i);
		if(billItemFormRow.find(".formItemId").attr("disabled") == "disabled"){
			break;
		}
		var billItemDisplayRow = $("#billItemDisplayRow_"+i);
		
		var itemId = billItemFormRow.find(".formItemId").val();
		var itemRate = formatAmount(menuIdPriceMap[itemId]);
		
		if(itemRate != undefined){
			billItemFormRow.find(".formItemRate").val(itemRate);
			billItemDisplayRow.find(".itemRate").text(itemRate);
		}
	}
}

//tested
function updateBillAmount(){
	//var totalAmount = $("#totalAmount").val();
	var totalAmount = parseInt($("#totalAmount").text());
	var discount = $("#discount").val();
	var netAmount = formatAmount(totalAmount - discount);
	$("#amount").val(netAmount);
	
	$("#discount").val(formatAmount(discount));
}

//tested
function formatAmount(val){
	return Number(val).toFixed(2); 
}
